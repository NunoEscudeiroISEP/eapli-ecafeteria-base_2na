/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.meals.RegisterMenuController;
import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.DishPrinter;
import eapli.ecafeteria.domain.kitchen.Quantity;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.Console;
import java.util.Date;

/**
 *
 * @author Daniel Correia
 */
public class RegisterMenuUI extends AbstractUI {

    private final RegisterMenuController theController = new RegisterMenuController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        Date initialDate = defineDate("initial");
        Date finalDate = defineDate("final");
        Date mealDate = new Date(initialDate.getYear(), initialDate.getMonth(), initialDate.getDate() - 1);
        Quantity qtd = new Quantity(0, 0, 0);
        if (this.theController.startRegisterMenu(initialDate, finalDate)) {
            for (int i = 0; i <= theController.MenuPeriod(); i++) {
                mealDate.setDate(mealDate.getDate() + 1);
                final Iterable<Dish> allDishes = this.theController.allDishes();
                if (!allDishes.iterator().hasNext()) {
                    System.out.println("There are no registered Dishes");
                } else {
                    boolean nextDay = false;
                    int resp = 0;
                    while (nextDay == false) {
                        System.out.println("Select a dish for " + mealDate.toString() + ":");
                        final SelectWidget<Dish> dishSelector = new SelectWidget<>(allDishes, new DishPrinter());
                        dishSelector.show();
                        final Dish selectedDish = dishSelector.selectedElement();
                        if (selectedDish != null) {
                            System.out.println("Select the meal type:");
                            final Iterable<MealType> mealTypes = this.theController.mealTypes();
                            if (!mealTypes.iterator().hasNext()) {
                                System.out.println("There are no registeres Meal Types");
                            } else {
                                MealType selectedMealType = null;
                                while (selectedMealType == null) {
                                    final SelectWidget<MealType> mealTypeSelector = new SelectWidget<>(mealTypes, new MealTypePrinter());
                                    mealTypeSelector.show();
                                    selectedMealType = mealTypeSelector.selectedElement();
                                    if (selectedMealType == null) {
                                        System.out.println("You have to specify the meal type.");
                                    }
                                }
                                try {
                                    Date newDate = new Date(mealDate.getYear(), mealDate.getMonth(), mealDate.getDate());
                                    this.theController.addMealToMenu(newDate, selectedDish, selectedMealType, qtd);
                                } catch (DataConcurrencyException | DataIntegrityViolationException ex) {
                                    System.out.println("You tried to enter a meal which already exists in the database.");
                                }
                            }
                            System.out.println("Do you want to add another meal?");
                            resp = Console.readInteger("1.Yes\n2.No\n");
                            if (resp == 2) {
                                nextDay = true;
                            }
                        } else {
                            System.out.println("You did not select any dish.\nDo you want to move to the next day?");
                            resp = Console.readInteger("1.Stay in this day\n2.Go to the next day\n");
                            if (resp == 2) {
                                nextDay = true;
                            }
                        }
                    }
                }
            }
            System.out.println("Do you want to make the menu public?");
            int respPublic = Console.readInteger("1.Yes\n2.No\n");
            if (respPublic == 1) {
                this.theController.makeMenuStateToPublished();
            }
            new MenuPrinter().visit(this.theController.getMenuToPrint());

            try {
                this.theController.registerMenu();
                System.out.println("\nThe menu has been successfully registered");
            } catch (DataConcurrencyException | DataIntegrityViolationException ex) {
                System.out.println("You tried to enter a menu which already exists in the database.");
            }

        }

        return true;
    }

    public Date defineDate(String typeDate) {
        System.out.println("Enter the " + typeDate + " date for the menu:");
        return Console.readDate("YYYY/MM/DD:");
    }

    @Override
    public String headline() {
        return "Create a new Menu";
    }

}
