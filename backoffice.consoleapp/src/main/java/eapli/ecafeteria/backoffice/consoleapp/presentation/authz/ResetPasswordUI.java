package eapli.ecafeteria.backoffice.consoleapp.presentation.authz;

import eapli.ecafeteria.application.authz.ResetPasswordController;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;
import eapli.util.Strings;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by vascopinho on 24/05/17.
 */
public class ResetPasswordUI extends AbstractUI {
    
    private final ResetPasswordController theController = new ResetPasswordController();
    
    @Override
    protected boolean doShow() {
        final List<SystemUser> list = new ArrayList<>();
        Iterable<SystemUser> iterable = this.theController.getAllUsers();
        if (!iterable.iterator().hasNext()) {
            System.out.println("There are no Registered Users");
        } else {
            int cont = 1;
            System.out.println("SELECT user to reset password\n");
            System.out.printf("%-6s%-60s%-20s\n", "Nยบ:", "Name", "Description");
            for (final SystemUser user : iterable) {
                list.add(user);
                System.out.printf("%2d %10s %30s %30s\n", cont, user.username(), user.name().firstName(), user.name().lastName());
                cont++;
            }
            final int option = Console.readInteger("Enter user to reset password");
            if (option == 0) {
                System.out.println("No organic unit selected");
            } else {
                SystemUser selected = list.get(option-1);
                try {
                    this.theController.resetPassword(selected);
                }catch (IllegalStateException e ){
                    System.out.println("Couldn't update password");
                } catch (DataIntegrityViolationException | DataConcurrencyException ex) {
                    Logger.getLogger(ResetPasswordUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        }
        return true;
    }

    @Override
    public String headline() {
        return "Reset Password";
    }
}
