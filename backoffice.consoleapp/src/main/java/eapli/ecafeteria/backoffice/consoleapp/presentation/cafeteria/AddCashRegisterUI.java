package eapli.ecafeteria.backoffice.consoleapp.presentation.cafeteria;

import eapli.ecafeteria.application.pos.CashRegisterController;
import eapli.ecafeteria.domain.cafeteria.OrganicUnit;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.Console;

/**
 *
 * @author Jorge Esteves
 */
public class AddCashRegisterUI extends AbstractUI {

    private final CashRegisterController theController = new CashRegisterController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        final String acronym = Console.readLine("Acronym");
        final String name = Console.readLine("Name");
        final String description = Console.readLine("Description");

        final SelectWidget<OrganicUnit> selector = new SelectWidget<>(this.theController.organicUnits(), 
                new OrganicUnitPrinter());
        selector.show();
        final OrganicUnit orga = selector.selectedElement();

        try {
            this.theController.registerCashRegister(acronym, name, description, orga);
        } catch (final DataIntegrityViolationException | DataConcurrencyException e) {
            System.out.println("That acronym is alredy in use.");
        }
        return false;
    }

    @Override
    public String headline() {
        return "Register Cash Register";
    }

}
