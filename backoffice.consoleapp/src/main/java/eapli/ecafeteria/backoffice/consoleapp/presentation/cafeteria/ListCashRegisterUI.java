/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.cafeteria;

import eapli.ecafeteria.application.pos.ListCashRegisterController;
import eapli.ecafeteria.domain.pos.CashRegister;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author Jorge Esteves
 */
class ListCashRegisterUI extends AbstractListUI<CashRegister>  {

    private ListCashRegisterController theController = new ListCashRegisterController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected Iterable<CashRegister> listOfElements() {
        return this.theController.listCashRegister();
    }

    @Override
    protected Visitor<CashRegister> elementPrinter() {
        return new CashRegisterPrinter();
    }

    @Override
    protected String elementName() {
        return "Cash Register";
    }
    
}
