package eapli.ecafeteria.backoffice.consoleapp.presentation.authz;

import eapli.ecafeteria.application.authz.SetUserAlertLimitController;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by vascopinho on 18/05/17.
 */
public class SetUserAlertLimitUI extends AbstractUI {

    private final SetUserAlertLimitController theController = new SetUserAlertLimitController();

    @Override
    protected boolean doShow() {
        final int limitInt;
        try {
            final String limit = Console.readLine("User budget limit:");

            limitInt = Integer.parseInt(limit);

        }catch (NullPointerException ex){
            Logger.getLogger(SetUserAlertLimitUI.class.getName()).log(Level.WARNING, "User input was not integer data type");
            return false;
        }

        try {
            this.theController.setUserLimits(limitInt);
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        } catch (final DataIntegrityViolationException | DataConcurrencyException e) {
            System.out.println("That username is already in use.");
        }

        return false;
    }

    @Override
    public String headline() {
        return "Set User Alert Limit";
    }
}
