package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.meals.RegisterDishController;
import eapli.ecafeteria.domain.meals.Allergen;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.Console;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Jorge Santos ajs@isep.ipp.pt
 */
public class RegisterDishUI extends AbstractUI {

    private final RegisterDishController theController = new RegisterDishController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        final Iterable<DishType> dishTypes = this.theController.dishTypes();

        final SelectWidget<DishType> selector = new SelectWidget<>(dishTypes, new DishTypePrinter());
        selector.show();
        final DishType theDishType = selector.selectedElement();

        final String name = Console.readLine("Name");

        final NutricionalInfoDataWidget nutricionalData = new NutricionalInfoDataWidget();

        nutricionalData.show();

        final double price = Console.readDouble("Price");

        final int addAllergenics = Console.readInteger("Add Allergenics? 1 - Yes, 0 - No");

        List<Allergen> allergenList = new ArrayList<>();

        if(addAllergenics == 1){
            int res;
            do{
                final Iterable<Allergen> allergens = this.theController.allergens();
                if (!allergens.iterator().hasNext()) {
                    System.out.println("There are no registered Allergenics");
                    return false;
                } else {
                    final SelectWidget<Allergen> selector2 = new SelectWidget<>(allergens, new AllergenPrinter());
                    selector2.show();
                    allergenList.add(selector2.selectedElement());
                }
                res = Console.readInteger("Add more Allergenics? 1 - Yes, 0 - No");
            }while (res == 1);
        }

        try {
            this.theController.registerDish(theDishType, name, nutricionalData.calories(), nutricionalData.salt(), price, allergenList);
        } catch (final DataIntegrityViolationException | DataConcurrencyException e) {
            System.out.println("You tried to enter a dish which already exists in the database.");
        }

        return false;
    }

    @Override
    public String headline() {
        return "Register Dish";
    }
}
