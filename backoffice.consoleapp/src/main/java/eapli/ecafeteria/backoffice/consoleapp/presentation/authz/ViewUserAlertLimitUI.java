package eapli.ecafeteria.backoffice.consoleapp.presentation.authz;

import eapli.ecafeteria.application.authz.SetUserAlertLimitController;
import eapli.ecafeteria.domain.authz.UserAlert;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;

/**
 * Created by vascopinho on 18/05/17.
 */
public class ViewUserAlertLimitUI extends AbstractUI {
    private final SetUserAlertLimitController theController = new SetUserAlertLimitController();

    @Override
    protected boolean doShow() {

        try {
            UserAlert userLimits = this.theController.getUserLimits();
            System.out.println(userLimits.toString());

        } catch (final DataIntegrityViolationException | DataConcurrencyException e) {
            System.out.println("That username is already in use.");
        }

        return false;
    }

    @Override
    public String headline() {
        return "View User Alert Limit";
    }
}
