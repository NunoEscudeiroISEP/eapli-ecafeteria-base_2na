/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.kitchen;

import eapli.ecafeteria.application.kitchen.ListMaterialController;
import eapli.ecafeteria.application.kitchen.RegisterBatchController;
import eapli.ecafeteria.application.kitchen.RegisterMaterialController;
import eapli.ecafeteria.application.meals.ListDishController;
import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.ListDishUI;
import eapli.ecafeteria.domain.kitchen.Material;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.MaterialRepository;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Daniel
 */
public class RegisterBatchUI extends AbstractUI {

    private final RegisterBatchController theController = new RegisterBatchController();
    
    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        // para alterar
//        Material material = null;
        final String batchNumber = Console.readLine("Batch Number:");
        final Date expirationDate = Console.readDate("Expiration Date (dd/mm/yyyy):","dd/mm/yyyy");
        final boolean confirmacao = Console.readBoolean("Confirm operation? (y/n)");
        if (confirmacao)
        {
        try {
            this.theController.registerBatch(batchNumber, expirationDate);
            System.out.println("Batch registed with success!");
            return true;
        } catch (final DataConcurrencyException | DataIntegrityViolationException e) {
            System.out.println("Error.");
        }
        }
        System.out.println("Operation canceled!");
        return false;
    }

    @Override
    public String headline() {
        return "Register Batch";
    }
}
