package eapli.ecafeteria.backoffice.consoleapp.presentation.kitchen;

import eapli.ecafeteria.application.kitchen.SetKitchenAlertLimitController;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by vascopinho on 04/05/17.
 */
public class SetKitchenAlertLimitUI extends AbstractUI {

    private final SetKitchenAlertLimitController theController = new SetKitchenAlertLimitController();

    @Override
    protected boolean doShow() {
        final int redInt;
        final int yellowInt;

        try {
            final String yellow = Console.readLine("Yellow limit:");
            final String red = Console.readLine("Red limit:");

            yellowInt = Integer.parseInt(red);
            redInt = Integer.parseInt(yellow);

        }catch (NullPointerException ex){
            Logger.getLogger(SetKitchenAlertLimitUI.class.getName()).log(Level.WARNING, "User input was not integer data type");
            return false;
        }

        try {
            this.theController.setKitchenLimits(yellowInt, redInt);
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        } catch (final DataIntegrityViolationException | DataConcurrencyException e) {
            System.out.println("That username is already in use.");
        }

        return false;
    }

    @Override
    public String headline() {
        return "Set Kitchen Alert Limit";
    }
}
