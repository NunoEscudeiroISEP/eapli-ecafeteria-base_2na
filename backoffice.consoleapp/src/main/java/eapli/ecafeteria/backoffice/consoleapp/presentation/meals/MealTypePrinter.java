/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author Daniel Correia
 */
public class MealTypePrinter implements Visitor<MealType>{
    
    @Override
    public void visit(MealType visitee) {
        System.out.printf("%-10s%-30s\n", visitee.id(), visitee.description());
    }

    @Override
    public void beforeVisiting(MealType visitee) {
        // nothing to do
    }

    @Override
    public void afterVisiting(MealType visitee) {
        // nothing to do
    }
}
