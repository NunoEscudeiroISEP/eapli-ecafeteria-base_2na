/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.kitchen;

import eapli.ecafeteria.application.kitchen.RegisterBatchController;
import eapli.ecafeteria.application.kitchen.SearchBatchController;
import eapli.ecafeteria.domain.kitchen.Batch;
import eapli.framework.actions.Action;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Daniel
 */
public class SearchBatchUI extends AbstractUI {
    
    private final SearchBatchController theController = new SearchBatchController();
    
    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        
        Iterable<Batch> batches = null;
        final boolean confirmacao = Console.readBoolean("Search by batch number? (y/n)");
        if (confirmacao)
        {
            try {
                final String batchNumber = Console.readLine("Batch Number:");
                batches = theController.searchBatchByNumber(batchNumber);
                Iterator iterador = batches.iterator();
                if (iterador.hasNext() == false)
                {
                    System.out.println("No batches found!");
                    return false;
                }
                while (iterador.hasNext())
                {
                    Batch batch = batches.iterator().next();
                    System.out.println("Batch Number: " + batch.getBatchnumber());
                    System.out.println("Batch Used Day: " + batch.getDayUsedString());
                    System.out.println("Batch Expiration Day: " + batch.getExpirationdateString());
                    System.out.println("-------------------------------------------------");
                    iterador.next();
                }
                return true;
            } catch (DataIntegrityViolationException ex) {
                Logger.getLogger(SearchBatchUI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (DataConcurrencyException ex) {
                Logger.getLogger(SearchBatchUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        final boolean confirmacao2 = Console.readBoolean("Check used batches on given day? (y/n)");
        if (confirmacao2)
        {
            try {
                final Date expirationDate = (Date) Console.readDate("Checks batches on day (dd/mm/yyyy):","dd/mm/yyyy");
                
                batches = theController.searchBatchByUsedDate(expirationDate);
                Iterator iterador = batches.iterator();
                if (iterador.hasNext() == false)
                {
                    System.out.println("No batches found!");
                    return false;
                }
                while (iterador.hasNext())
                {
                    Batch batch = batches.iterator().next();
                    System.out.println("Batch Number: " + batch.getBatchnumber());
                    System.out.println("Batch Used Day: " + batch.getDayUsedString());
                    System.out.println("Batch Expiration Day: " + batch.getExpirationdateString());
                    System.out.println("-------------------------------------------------");
                    iterador.next();
                }
                return true;
            } catch (DataIntegrityViolationException ex) {
                Logger.getLogger(SearchBatchUI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (DataConcurrencyException ex) {
                Logger.getLogger(SearchBatchUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return false;
    }

    @Override
    public String headline() {
        return "Search Batch";
    }
    
}
