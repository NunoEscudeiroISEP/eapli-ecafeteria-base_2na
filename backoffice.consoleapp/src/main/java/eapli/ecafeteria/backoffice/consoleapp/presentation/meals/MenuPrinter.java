/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author Daniel Correia
 */
public class MenuPrinter implements Visitor<Menu> {

    @Override
    public void visit(Menu visitee) {
        System.out.printf("%-30s to %-25s Publish: %-10s\n", visitee.initialDate().toString(), visitee.finalDate().toString(), String.valueOf(visitee.isPublished()));
        for (Meal meal : visitee.meals()) {
            new MealPrinter().visit(meal);           
        }
    }
    
}
