/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.meals.PublishMenuController;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Daniel Correia
 */
public class PublishMenuUI extends AbstractUI {

    private final PublishMenuController theController = new PublishMenuController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {

        final Iterable<Menu> allMenus = this.theController.allInProgressMenus();
        if (!allMenus.iterator().hasNext()) {
            System.out.println("There are no registered Menus");
        } else {
            final SelectWidget<Menu> selector = new SelectWidget<>(allMenus, new MenuPrinter());
            selector.show();
            final Menu updtMenu = selector.selectedElement();
            if (updtMenu != null) {
                try {
                    this.theController.changeMenuStateToPublished(updtMenu);
                    System.out.println("Status changed successfully");
                } catch (DataConcurrencyException ex) {
                    System.out.println("It is not possible publish the menu because it was changed by another user");
                } catch (DataIntegrityViolationException ex) {
                    //should not happen!
                    Logger.getLogger(ActivateDeactivateDishTypeUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                System.out.println("No menus have been published");
            }
        }
        return true;
    }

    @Override
    public String headline() {
        return "Publish Menu.";
    }

}
