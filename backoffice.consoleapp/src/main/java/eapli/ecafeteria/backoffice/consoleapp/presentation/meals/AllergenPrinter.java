/**
 *
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.domain.meals.Allergen;
import eapli.framework.visitor.Visitor;

/**
 * @author 1141045@isep.ipp.pt
 * @author 1090956@isep.ipp.pt
 */
class AllergenPrinter implements Visitor<Allergen> {

    @Override
    public void visit(Allergen visitee) {
        System.out.printf("%-30s\n", visitee.getName());
    }
}
