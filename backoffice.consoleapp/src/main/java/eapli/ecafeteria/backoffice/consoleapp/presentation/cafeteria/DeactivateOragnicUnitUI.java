package eapli.ecafeteria.backoffice.consoleapp.presentation.cafeteria;

import eapli.ecafeteria.application.cafeteria.DeactivateOragnicUnitController;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.cafeteria.OrganicUnit;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by vascopinho on 23/05/17.
 */
public class DeactivateOragnicUnitUI extends AbstractUI {

    private final DeactivateOragnicUnitController theController = new DeactivateOragnicUnitController();



    @Override
    protected boolean doShow() {
        final List<OrganicUnit> list = new ArrayList<>();
        final Iterable<OrganicUnit> iterable = this.theController.activeOrganicUnits();

        if (!iterable.iterator().hasNext()) {
            System.out.println("There is no registered Organic Unit");
        } else {
            int cont = 1;
            System.out.println("SELECT Organic Unit to deactivate\n");
            System.out.printf("%-6s%-60s%-20s\n", "Nº:", "Name", "Description");
            for (final OrganicUnit organicUnit : iterable) {
                list.add(organicUnit);
                System.out.printf("%-6d%-60s%-20s\n", cont, organicUnit.name(), organicUnit.description());
                cont++;
            }
            final int option = Console.readInteger("Enter Organic Unit nº to deactivate or 0 to finish ");
            if (option == 0) {
                System.out.println("No organic unit selected");
            } else {
                try {
                    this.theController.deactivateOrganicUnit(list.get(option - 1));
                } catch (DataIntegrityViolationException | DataConcurrencyException ex) {
                    Logger.getLogger(DeactivateOragnicUnitUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return true;


    }

    @Override
    public String headline() {
        return "Deactivate Organic Unit";
    }
}
