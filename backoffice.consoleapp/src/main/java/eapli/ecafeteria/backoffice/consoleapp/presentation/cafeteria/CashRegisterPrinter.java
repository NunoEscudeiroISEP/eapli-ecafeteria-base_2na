
package eapli.ecafeteria.backoffice.consoleapp.presentation.cafeteria;

import eapli.ecafeteria.domain.pos.CashRegister;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author Jorge Esteves
 */
public class CashRegisterPrinter implements Visitor<CashRegister> {

    @Override
    public void visit(CashRegister visitee) {
        System.out.printf("%-10s%-30s%-4s\n", visitee.id(), visitee.name(), visitee.organic(), String.valueOf(visitee.isOpen()));
    }

    @Override
    public void beforeVisiting(CashRegister visitee) {
        // nothing to do
    }

    @Override
    public void afterVisiting(CashRegister visitee) {
        // nothing to do
    }

}
