package eapli.ecafeteria.backoffice.consoleapp.presentation.kitchen;

import eapli.ecafeteria.application.kitchen.SetKitchenAlertLimitController;
import eapli.ecafeteria.domain.kitchen.KitchenAlert;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by vascopinho on 18/05/17.
 */
public class ViewKitchenAlertLimitUI extends AbstractUI{

    private final SetKitchenAlertLimitController theController = new SetKitchenAlertLimitController();

    @Override
    protected boolean doShow() {

        try {
            KitchenAlert kitchenLimits = this.theController.getKitchenLimits();
            System.out.println(kitchenLimits.toString());

        } catch (final DataIntegrityViolationException | DataConcurrencyException e) {
            System.out.println("That username is already in use.");
        }

        return false;
    }

    @Override
    public String headline() {
        return "View Kitchen Alert Limit";
    }
}
