/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.meals.EditMenuController;
import eapli.ecafeteria.domain.kitchen.Quantity;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.Console;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Daniel Correia
 */
public class EditMenuUI extends AbstractUI {

    private final EditMenuController theController = new EditMenuController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        final Iterable<Menu> menus = this.theController.allInProgressMenus();
        if (!menus.iterator().hasNext()) {
            System.out.println("There are no registered Menus");
        } else {
            final SelectWidget<Menu> selector = new SelectWidget<>(menus, new MenuPrinter());
            selector.show();
            final Menu selectedMenu = selector.selectedElement();
            if (selectedMenu != null) {
                try {
                    this.theController.selectMenu(selectedMenu);
                } catch (CloneNotSupportedException ex) {
                    Logger.getLogger(EditMenuUI.class.getName()).log(Level.SEVERE, null, ex);
                }
                boolean edit = true;
                while (edit) {
                    int resp = Console.readInteger("1.Edit initial date\n2.Edit final date\n3.Edit meals");
                    switch (resp) {
                        case 1: {
                            editInitialDate();
                            edit = continueEdit();
                            break;
                        }
                        case 2: {
                            editFinalDate();
                            edit = continueEdit();
                            break;
                        }
                        case 3: {
                            EditMeals();
                            edit = continueEdit();
                            break;
                        }
                    }
                }

                System.out.println("Do you want to make the menu public?");
                int respPublic = Console.readInteger("1.Yes\n2.No\n");
                if (respPublic == 1) {
                    this.theController.makeMenuStateToPublished();
                }
                new MenuPrinter().visit(this.theController.getMenuToPrint());
                System.out.println("Do you want to save the changes?");
                int respSave = Console.readInteger("1.Yes\n2.No\n");
                if (respSave == 1) {
                    try {
                        this.theController.editMenu();
                        System.out.println("Menu edited successfully");
                    } catch (DataConcurrencyException ex) {
                        System.out.println("It is not possible publish the menu because it was changed by another user");
                    } catch (DataIntegrityViolationException ex) {
                        //should not happen!
                        Logger.getLogger(ActivateDeactivateDishTypeUI.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    System.out.println("No menu have been edited");
                }
            } else {
                System.out.println("No menu have been edited");
            }
        }
        return true;
    }

    public boolean continueEdit() {
        System.out.println("Do you want to continue editing?");
        int resp = Console.readInteger("1.Yes\n2.No\n");
        if (resp == 2) {
            return false;
        }
        return true;
    }

    public void editInitialDate() {
        System.out.println("When editing the initial date, all meals that have a previous date will be removed!\n Are you sure you want to continue?");
        int resp = Console.readInteger("1.Yes\n2.No\n");
        if (resp == 1) {
            Date initialDate = defineDate("initial");
            this.theController.setInitialDate(initialDate);
        }
    }

    public void editFinalDate() {
        System.out.println("When editing the final date, all meals that have a posterior date will be removed!\n Are you sure you want to continue?");
        int resp = Console.readInteger("1.Yes\n2.No\n");
        if (resp == 1) {
            Date finalDate = defineDate("final");
            this.theController.setFinalDate(finalDate);
        }
    }

    public void EditMeals() {
        System.out.println("Do you want to add or remove meals?");
        int resp = Console.readInteger("1.Remove meals\n2.Add meals\n");
        if (resp == 1) {
            Iterable<Meal> mealsofMenu = this.theController.allMealsfromMenu();
            final SelectWidget<Meal> selector = new SelectWidget<>(mealsofMenu, new MealPrinter());
            selector.show();
            final Meal selectedMeal = selector.selectedElement();
            if (selectedMeal != null) {
                this.theController.removeMeal(selectedMeal);
            } else {
                System.out.println("You do not select any meal.");
            }
        } else {
            Date initialDate = this.theController.MenuInitialDate();
            Date mealDate = new Date(initialDate.getYear(), initialDate.getMonth(), initialDate.getDate() - 1);
            Quantity qtd = new Quantity(0, 0, 0);
            for (int i = 0; i <= theController.MenuPeriod(); i++) {
                mealDate.setDate(mealDate.getDate() + 1);
                final Iterable<Dish> allDishes = this.theController.allDishes();
                if (!allDishes.iterator().hasNext()) {
                    System.out.println("There are no registered Dishes");
                } else {
                    boolean nextDay = false;
                    resp = 0;
                    while (nextDay == false) {
                        System.out.println("Select a dish for " + mealDate.toString() + ":");
                        final SelectWidget<Dish> dishSelector = new SelectWidget<>(allDishes, new DishPrinter());
                        dishSelector.show();
                        final Dish selectedDish = dishSelector.selectedElement();
                        if (selectedDish != null) {
                            System.out.println("Select the meal type:");
                            final Iterable<MealType> mealTypes = this.theController.mealTypes();
                            if (!mealTypes.iterator().hasNext()) {
                                System.out.println("There are no registeres Meal Types");
                            } else {
                                MealType selectedMealType = null;
                                while (selectedMealType == null) {
                                    final SelectWidget<MealType> mealTypeSelector = new SelectWidget<>(mealTypes, new MealTypePrinter());
                                    mealTypeSelector.show();
                                    selectedMealType = mealTypeSelector.selectedElement();
                                    if (selectedMealType == null) {
                                        System.out.println("You have to specify the meal type.");
                                    }
                                }
                                try {
                                    Date newDate = new Date(mealDate.getYear(), mealDate.getMonth(), mealDate.getDate());
                                    this.theController.addMealToMenu(newDate, selectedDish, selectedMealType, qtd);
                                } catch (DataConcurrencyException | DataIntegrityViolationException ex) {
                                    System.out.println("You tried to enter a meal which already exists in the database.");
                                }
                            }
                            System.out.println("Do you want to add another meal?");
                            resp = Console.readInteger("1.Yes\n2.No\n");
                            if (resp == 2) {
                                nextDay = true;
                            }
                        } else {
                            System.out.println("You did not select any dish.\nDo you want to move to the next day?");
                            resp = Console.readInteger("1.Stay in this day\n2.Go to the next day\n");
                            if (resp == 2) {
                                nextDay = true;
                            }
                        }
                    }
                }
            }
        }
    }

    public Date defineDate(String typeDate) {
        System.out.println("Enter the " + typeDate + " date for the menu:");
        return Console.readDate("YYYY/MM/DD:");
    }

    @Override
    public String headline() {
        return "Edit Menu";
    }

}
