/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.meals.Menu;
import eapli.ecafeteria.persistence.MenuRepository;
import java.util.Date;

/**
 *
 * @author Daniel Correia
 */
public class JpaMenuRepository extends CafeteriaJpaRepositoryBase<Menu, Long> implements MenuRepository {
    
    @Override
    public Menu findByDate(Date inicialDate, Date finalDate) {
         return matchOne("e.initialDate='"+inicialDate.toString()+ "' AND e.finalDate='"+finalDate.toString()+"'");
    }
    
    @Override
    public Iterable<Menu> findAllPublished(){
        return match("e.statePublish=true");
    }

    @Override
    public Iterable<Menu> findAllInProgress() {
        return match("e.stateinProgress=true");
    }

    @Override
    public Menu findByDay(Date day) {
         return matchOne("e.initialDate>='"+day+ "' AND e.finalDate<='"+day+"'");
    }
    
}
