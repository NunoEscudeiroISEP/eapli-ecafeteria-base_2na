/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.pos.Shift;
import eapli.ecafeteria.persistence.ShiftRepository;
import java.util.Date;

/**
 *
 * @author Carlos Pinho & Jorge Esteves
 */
public class JpaShiftRepository extends CafeteriaJpaRepositoryBase<Shift, Long>
        implements ShiftRepository {

    @Override
    public Shift findByDate(Date openDate) {
        return matchOne("e.openDate:=a", "a", openDate);
    }
    
    

    
    
    
}