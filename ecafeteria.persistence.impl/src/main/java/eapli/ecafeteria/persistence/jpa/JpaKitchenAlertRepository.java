package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.kitchen.KitchenAlert;
import eapli.ecafeteria.persistence.KitchenAlertRepository;

/**
 * Created by vascopinho on 18/05/17.
 */
public class JpaKitchenAlertRepository extends CafeteriaJpaRepositoryBase<KitchenAlert, Long> implements KitchenAlertRepository {


    @Override
    public KitchenAlert getKitchenAlert() {
       return (KitchenAlert) this.entityManager().createQuery("Select e from " + this.entityClass.getSimpleName() + " e order by e.insertionDate desc").setMaxResults(1).getSingleResult();
    }
}
