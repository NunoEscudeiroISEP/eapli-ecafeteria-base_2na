package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.meals.Allergen;
import eapli.ecafeteria.persistence.AllergenRepository;
import eapli.framework.domain.Designation;


/**
 * @author 1141045@isep.ipp.pt
 * @author 1090956@isep.ipp.pt
 */
public class JpaAlergenRepository extends CafeteriaJpaRepositoryBase<Allergen, Designation> implements AllergenRepository{


    @Override
    public Allergen findByName(Designation name) {
        return matchOne("e.name='" + name + "'");
    }
}
