/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.pos.Complaint;
import eapli.ecafeteria.persistence.ComplaintRepository;

/**
 *
 * @author Topa
 */
class JpaComplainsRepository extends CafeteriaJpaRepositoryBase<Complaint, Long> implements ComplaintRepository {

    @Override
    public Complaint getCommentByName(String name) {
         return matchOne("e.name=:name", "name", name);
    }

    @Override
    public Complaint getCommentById(Long id) {
        return matchOne("e.id=:id", "id", id);
    }

  
}
