package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.authz.UserAlert;
import eapli.ecafeteria.persistence.UserAlertRepository;

/**
 * Created by vascopinho on 18/05/17.
 */
public class JpaUserAlertRepository extends CafeteriaJpaRepositoryBase<UserAlert, Long> implements UserAlertRepository {


    @Override
    public UserAlert getUserAlert() {
        return (UserAlert) this.entityManager().createQuery("Select e from " + this.entityClass.getSimpleName() + " e order by e.insertionDate desc").setMaxResults(1).getSingleResult();
    }
}