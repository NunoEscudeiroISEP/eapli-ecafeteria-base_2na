
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;
import eapli.ecafeteria.domain.pos.CashRegister;
import eapli.ecafeteria.persistence.CashRegisterRepository;
/**
 *
 * @author CarlosPinho (1980380)
 */
public class JpaCashRegisterRepository extends CafeteriaJpaRepositoryBase<CashRegister, Long>
        implements CashRegisterRepository{
    
    /**
     *
     * @param acronym
     * @return
     */
    @Override
    public CashRegister findByAcronym(String acronym) {
        return matchOne("e.acronym=:a", "a", acronym);
    }
   
    @Override
    public Iterable<CashRegister> closedCashRegisters(){
        return match("e.open=false");
    }
   
   
}