package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.persistence.*;

/**
 *
 * Created by nuno on 21/03/16.
 */
public class JpaRepositoryFactory implements RepositoryFactory {

    @Override
    public UserRepository users(boolean autoTx) {
        return new JpaUserRepository(autoTx);
    }

    @Override
    public DishTypeRepository dishTypes() {
        return new JpaDishTypeRepository();
    }

    @Override
    public OrganicUnitRepository organicUnits() {
        return new JpaOrganicUnitRepository();
    }

    @Override
    public JpaCafeteriaUserRepository cafeteriaUsers(boolean autoTx) {
        return new JpaCafeteriaUserRepository(autoTx);
    }

    @Override
    public SignupRequestRepository signupRequests(boolean autoTx) {
        return new JpaSignupRequestRepository(autoTx);
    }

    @Override
    public DishRepository dishes() {
        return new JpaDishRepository();
    }

    @Override
    public MaterialRepository materials() {
        return new JpaMaterialRepository();
    }

    @Override
    public BatchRepository batches() {
        return new JpaBatchRepository();
    }

    @Override
    public MealRepository meals() {
        return new JpaMealRepository();
    }

    @Override
    public MealTypeRepository mealTypes() {
        return new JpaMealTypeRepository();
    }

    @Override
    public BookingRepository bookings() {
        return new JpaBookingRepository();
    }

    @Override
    public AllergenRepository allergens() {
        return new JpaAlergenRepository();
    }

    @Override
    public KitchenAlertRepository kitchenAlert() {
        return new JpaKitchenAlertRepository();
    }

    @Override
    public UserAlertRepository userAlert() {
        return new JpaUserAlertRepository();
    }

    @Override
    public MenuRepository menus() {
        return new JpaMenuRepository();
    }
    
     @Override
    public CashRegisterRepository cashRegisters() {
        return new JpaCashRegisterRepository();
    }
    

     @Override
    public ShiftRepository shifts() {
        return new JpaShiftRepository();
    }

    @Override
    public ComplaintRepository complaints() {
        return new JpaComplainsRepository();
    }

    @Override
    public CommentRepository comments() {
        return new JpaCommentRepository();
    }
    
    
    
}
