/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.kitchen.Batch;
import eapli.ecafeteria.persistence.BatchRepository;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Daniel
 */
public class JpaBatchRepository extends CafeteriaJpaRepositoryBase<Batch, Long> implements BatchRepository{


    @Override
    public Iterable<Batch> findByUsedDate(Date date) {
        return match("e.dayUsed='"+ date +"'");
    }

    @Override
    public Iterable<Batch> findByBatchNumber(String batch) {
        return match("e.batchnumber='"+batch+"'");
    }
    
}
