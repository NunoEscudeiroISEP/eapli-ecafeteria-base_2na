/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.pos.Comment;
import eapli.ecafeteria.persistence.CommentRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;

/**
 *
 * @author Topa
 */
public class InMemoryCommentRepository extends InMemoryRepository<Comment, Long> implements CommentRepository{

    

    @Override
    public Comment getCommentByName(String name) {
       return matchOne(e -> e.id().equals(name));
    }

   

    @Override
    protected Long newPK(Comment entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    

    

   

    

   
       
}
