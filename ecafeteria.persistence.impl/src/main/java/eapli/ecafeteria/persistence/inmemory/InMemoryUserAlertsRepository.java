package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.authz.UserAlert;
import eapli.ecafeteria.persistence.UserAlertRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;

/**
 * Created by vascopinho on 18/05/17.
 */
public class InMemoryUserAlertsRepository extends InMemoryRepository<UserAlert,Long> implements UserAlertRepository {
    @Override
    public UserAlert getUserAlert() {
        return first();
    }

    @Override
    protected Long newPK(UserAlert entity) {
        return null;
    }
}
