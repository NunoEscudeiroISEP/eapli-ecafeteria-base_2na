/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.meals.Booking;
import eapli.ecafeteria.persistence.BookingRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepositoryWithLongPK;

/**
 *
 * @author marci
 */
public class InMemoryBookingRepository extends InMemoryRepositoryWithLongPK<Booking> implements BookingRepository{

    @Override
    public Iterable<Booking> getAllBookingsFromUser(CafeteriaUser user) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean ChangeBookingStatusToDelivered(Booking booking) {
        return booking.status().changeToDelivered();
    }

    @Override
    public boolean ChangeBookingStatusToCAncelled(Booking booking) {
        return booking.status().changeToCancelled();
    }
    
    
    
}
