/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.pos.Complaint;
import eapli.ecafeteria.persistence.ComplaintRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;

/**
 *
 * @author Topa
 */
public class InMemoryComplainsRepository extends InMemoryRepository<Complaint, Long> implements ComplaintRepository{

    @Override
    public Complaint getCommentByName(String name) {
       return matchOne(e -> e.id().equals(name));
    }

    @Override
    public Complaint getCommentById(Long id) {
         return matchOne(e -> e.longPk().equals(id));
    }

    @Override
    protected Long newPK(Complaint entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}