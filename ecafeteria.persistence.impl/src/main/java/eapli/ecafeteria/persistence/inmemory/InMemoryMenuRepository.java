/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.meals.Menu;
import eapli.ecafeteria.persistence.MenuRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepositoryWithLongPK;
import java.util.Date;

/**
 *
 * @author Daniel Correia
 */
public class InMemoryMenuRepository extends InMemoryRepositoryWithLongPK<Menu> implements MenuRepository {

    @Override
    public Menu findByDate(Date initialDate, Date finalDate) {
        return matchOne(e -> e.initialDate().equals(initialDate) && e.finalDate().equals(finalDate));
    }
    
    @Override
    public Iterable<Menu> findAllPublished() {
        return match(e -> e.isPublished());
    }

    @Override
    public Iterable<Menu> findAllInProgress() {
       return match(e -> e.isInProgress());
    }

    @Override
    public Menu findByDay(Date day) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
