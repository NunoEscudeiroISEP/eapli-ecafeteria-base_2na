/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.kitchen.Batch;
import eapli.ecafeteria.persistence.BatchRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepositoryWithLongPK;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Daniel
 */
public class InMemoryBatchRepository extends InMemoryRepositoryWithLongPK<Batch> implements BatchRepository{

    @Override
    public Iterable<Batch> findByBatchNumber(String batch) {
        return match(e -> e.getBatchnumber().equals(batch));
    }

    @Override
    public Iterable<Batch> findByUsedDate(Date date) {
        return match(e -> e.getDayUsed().equals(date));
    }
    
}
