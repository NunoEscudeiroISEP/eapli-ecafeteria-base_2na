package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.kitchen.KitchenAlert;
import eapli.ecafeteria.persistence.KitchenAlertRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;

/**
 * Created by vascopinho on 18/05/17.
 */
public class InMemoryKitchenAlertsRepository extends InMemoryRepository<KitchenAlert,Long> implements KitchenAlertRepository {
    @Override
    public KitchenAlert getKitchenAlert() {
        return first();
    }

    @Override
    protected Long newPK(KitchenAlert entity) {
        return null;
    }
}
