package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.bootstrapers.ECafeteriaBootstraper;
import eapli.ecafeteria.persistence.*;

/**
 *
 * Created by nuno on 20/03/16.
 */
public class InMemoryRepositoryFactory implements RepositoryFactory {

    static {
        // only needed because of the in memory persistence
        new ECafeteriaBootstraper().execute();
    }

    @Override
    public UserRepository users(boolean tx) {
        return new InMemoryUserRepository();
    }

    @Override
    public DishTypeRepository dishTypes() {
        return new InMemoryDishTypeRepository();
    }

    @Override
    public OrganicUnitRepository organicUnits() {
        return new InMemoryOrganicUnitRepository();
    }

    @Override
    public CafeteriaUserRepository cafeteriaUsers(boolean tx) {

        return new InMemoryCafeteriaUserRepository();
    }

    @Override
    public SignupRequestRepository signupRequests(boolean tx) {
        return new InMemorySignupRequestRepository();
    }

    @Override
    public DishRepository dishes() {
        return new InMemoryDishRepository();
    }

    @Override
    public MaterialRepository materials() {
        return new InMemoryMaterialRepository();
    }

    @Override
    public BatchRepository batches() {
        return new InMemoryBatchRepository();
    }

    @Override
    public MealRepository meals() {
        return new InMemoryMealRepository();
    }

    @Override
    public MealTypeRepository mealTypes() {
        return new InMemoryMealTypeRepository();
    }
    
    @Override
    public MenuRepository menus(){
        return new InMemoryMenuRepository();
    }

    @Override
    public BookingRepository bookings() {
        return new InMemoryBookingRepository();
    }

    @Override
    public AllergenRepository allergens(){
        return new InMemoryAllergenRepository();
    }

    @Override
    public KitchenAlertRepository kitchenAlert() { return new InMemoryKitchenAlertsRepository(); }

    @Override
    public UserAlertRepository userAlert() {return new InMemoryUserAlertsRepository();}
    
     @Override
    public CashRegisterRepository cashRegisters() {
        return new InMemoryCashRegisterRepository();}
    
    
    @Override
    public ShiftRepository shifts() {
        return new InMemoryShiftRepository();}

    @Override
    public ComplaintRepository complaints() {
        return new InMemoryComplainsRepository();
    }

    @Override
    public CommentRepository comments() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   

  

   
    
    
}
