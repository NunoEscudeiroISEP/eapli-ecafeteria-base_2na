package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.meals.Allergen;
import eapli.ecafeteria.persistence.AllergenRepository;
import eapli.framework.domain.Designation;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;

/**
 * @author 1141045@isep.ipp.pt
 * @author 1090956@isep.ipp.pt
 */
public class InMemoryAllergenRepository extends InMemoryRepository<Allergen, Designation> implements AllergenRepository {

    @Override
    public Allergen findByName(Designation name) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected Designation newPK(Allergen entity) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
