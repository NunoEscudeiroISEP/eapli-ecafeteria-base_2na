/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eapli.ecafeteria.persistence.inmemory;
import eapli.ecafeteria.domain.pos.CashRegister;
import eapli.ecafeteria.persistence.CashRegisterRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepositoryWithLongPK;

/**
 *
 * @author CarlosPinho (1980380) & Jorge Esteves
 */
    
public class InMemoryCashRegisterRepository extends InMemoryRepositoryWithLongPK<CashRegister> implements CashRegisterRepository
{
    @Override
    public CashRegister findByAcronym(String acronym) {
        return matchOne(e -> e.id().equals(acronym));
    }
   
    @Override
    public Iterable<CashRegister> closedCashRegisters(){
        return match(e -> e.isOpen());
    }
    
}