/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;


import eapli.ecafeteria.domain.pos.Shift;
import eapli.ecafeteria.persistence.ShiftRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;
import java.util.Date;

/**
 *
 * @author Carlos Pinho & Jorge Esteves
 */
public class InMemoryShiftRepository extends InMemoryRepository<Shift, Long> 
        implements ShiftRepository {

    @Override
    public Shift findByDate(Date openDate) {
        return (Shift) match(e -> e.openDate().equals(openDate));
    }

    @Override
    public Iterable<Shift> findAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected Long newPK(Shift entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    
}