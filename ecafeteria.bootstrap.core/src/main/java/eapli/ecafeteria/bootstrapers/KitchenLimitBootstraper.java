package eapli.ecafeteria.bootstrapers;

import eapli.ecafeteria.application.kitchen.SetKitchenAlertLimitController;
import eapli.framework.actions.Action;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;

/**
 * Created by vascopinho on 18/05/17.
 */
public class KitchenLimitBootstraper implements Action {
    @Override
    public boolean execute() {
        register(75,90);
        return false;
    }

    private void register(int yellow, int red) {
        final SetKitchenAlertLimitController controller = new SetKitchenAlertLimitController();
        try {
            controller.setKitchenLimits(red,yellow);
        } catch (DataIntegrityViolationException | DataConcurrencyException e) {
            e.printStackTrace();
        }
    }
}
