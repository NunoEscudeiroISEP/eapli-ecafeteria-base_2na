package eapli.ecafeteria.bootstrapers;

import eapli.ecafeteria.application.pos.CashRegisterController;
import eapli.ecafeteria.domain.cafeteria.OrganicUnit;
import eapli.ecafeteria.persistence.OrganicUnitRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.actions.Action;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.logging.Logger;

/**
 *
 * @author Jorge Esteves
 */
public class CashRegisterBootstrapper implements Action {

    @Override
    public boolean execute() {

        final OrganicUnitRepository orgaUnitRepo = PersistenceContext.repositories().organicUnits();
        final OrganicUnit unit = orgaUnitRepo.findByAcronym("ISEP");

        register("cr#1", "Cash Register 1", "POSTO 1", unit);
        register("cr#2", "Cash Register 2", "POSTO 2", unit);
        register("cr#3", "Cash Register 3", "POSTO 3", unit);
        register("cr#4", "Cash Register 4", "POSTO 4", unit);

        return false;
    }

    /**
     *
     */
    private void register(String acronym, String name, String description, OrganicUnit orga) {
        final CashRegisterController controller = new CashRegisterController();
        try {
            controller.registerCashRegister(acronym, name, description, orga);
        } catch (final DataIntegrityViolationException | DataConcurrencyException e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
            Logger.getLogger(ECafeteriaBootstraper.class.getSimpleName())
                    .info("EAPLI-DI001: Exception during bootstrapping: assuming existing record.");
        }
    }
}
