/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.bootstrapers;

import eapli.ecafeteria.application.meals.RegisterMealController;
import eapli.ecafeteria.domain.kitchen.Quantity;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.ecafeteria.persistence.MealTypeRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.actions.Action;
import eapli.framework.domain.Designation;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.Date;
import java.util.logging.Logger;

/**
 *
 * @author Daniel Correia
 */
public class MealBootstraper implements Action {

    @Override
    public boolean execute() {
        
        final DishRepository dishRepository = PersistenceContext.repositories().dishes();
        final Dish dish1 = dishRepository.findByName(Designation.valueOf("tofu grelhado"));
        final Dish dish2 = dishRepository.findByName(Designation.valueOf("lentilhas salteadas"));
        final Dish dish3 = dishRepository.findByName(Designation.valueOf("bacalhau à braz"));
        final Dish dish4 = dishRepository.findByName(Designation.valueOf("lagosta suada"));
        final Dish dish5 = dishRepository.findByName(Designation.valueOf("picanha"));
        final Dish dish6 = dishRepository.findByName(Designation.valueOf("costeleta à salsicheiro"));

        
        final MealTypeRepository mealTypeRepository = PersistenceContext.repositories().mealTypes();
        final MealType lunch = mealTypeRepository.findByAcronym("lunch");
        final MealType dinner = mealTypeRepository.findByAcronym("dinner");

        final Date date1 = new Date(117, 5, 12);
        final Date date2 = new Date(117, 5, 13);
        final Date date3 = new Date(117, 5, 14);
        final Date date4 = new Date(117, 5, 15);
        final Date date5 = new Date(117, 5, 16);
        final Date date6 = new Date(117, 5, 19);
        final Date date7 = new Date(117, 5, 20);
        final Date date8 = new Date(117, 5, 21);
        final Date date9 = new Date(117, 5, 22);
        final Date date10 = new Date(117, 5, 23);
        Quantity qtd=new Quantity(10,10,10);
        
        register(date1, dish2, dinner, qtd);
        register(date2, dish1, lunch, qtd);
        register(date3, dish4, dinner, qtd);
        register(date4, dish3, lunch, qtd);
        register(date5, dish6, dinner, qtd);
        register(date6, dish6, dinner, qtd);
        register(date7, dish3, lunch, qtd);
        register(date8, dish1, dinner, qtd);
        register(date9, dish4, lunch, qtd);
        register(date10, dish2, dinner, qtd);
    
        return false;
    }

    /**
     *
     */
    private void register(Date date, Dish dish, MealType mealType, Quantity qtd) {
        final RegisterMealController controller = new RegisterMealController();
        try {
            controller.registerMeal(date, dish, mealType, qtd);
        } catch (final DataIntegrityViolationException | DataConcurrencyException e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
            Logger.getLogger(ECafeteriaBootstraper.class.getSimpleName())
                    .info("EAPLI-DI001: bootstrapping existing record");
        }
    }

}
