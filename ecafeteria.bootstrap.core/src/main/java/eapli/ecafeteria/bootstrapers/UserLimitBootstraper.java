package eapli.ecafeteria.bootstrapers;

import eapli.ecafeteria.application.authz.SetUserAlertLimitController;
import eapli.framework.actions.Action;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;

/**
 * Created by vascopinho on 18/05/17.
 */
public class UserLimitBootstraper implements Action {
    @Override
    public boolean execute() {
        register(3);
        return false;
    }

    private void register(int userLimit) {
        final SetUserAlertLimitController controller = new SetUserAlertLimitController();
        try {
            controller.setUserLimits(userLimit);
        } catch (DataIntegrityViolationException | DataConcurrencyException e) {
            e.printStackTrace();
        }
    }
}
