/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.bootstrapers;

import eapli.ecafeteria.application.meals.BookingMealController;
import eapli.ecafeteria.domain.authz.Username;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.cafeteria.OrganicUnit;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.OrganicUnitRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.UserRepository;
import eapli.framework.actions.Action;
import eapli.framework.domain.Money;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.ArrayList;
import java.sql.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author marci
 */
public class BookingMealBootstraper implements Action {
    
    @Override
    public boolean execute() {
        final OrganicUnitRepository unitRepo = PersistenceContext.repositories().organicUnits();
        final UserRepository systemUserRepo = PersistenceContext.repositories().users(true);
        final CafeteriaUserRepository userRepo = PersistenceContext.repositories().cafeteriaUsers(true);
        
        OrganicUnit unit = unitRepo.findByAcronym("ISEP");
        Username username = new Username("900330");
        CafeteriaUser user = userRepo.findByUsername(username);
        user.balance().addMoney(Money.euros(100));
        Date date = Date.valueOf("2017-06-13");;
        
        try {
            userRepo.save(user);
        } catch (DataConcurrencyException ex) {
            Logger.getLogger(BookingMealBootstraper.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DataIntegrityViolationException ex) {
            Logger.getLogger(BookingMealBootstraper.class.getName()).log(Level.SEVERE, null, ex);
        }
        register(user, date);
        return false;
    }
    
    private void register(CafeteriaUser user, Date date) {
        final BookingMealController ctrl = new BookingMealController();
        ArrayList<Meal> meals = (ArrayList<Meal>) ctrl.getMealsByDate(date);
        try {
            ctrl.bookingMeal(meals.get(0));
        } catch (DataConcurrencyException ex) {
            Logger.getLogger(BookingMealBootstraper.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DataIntegrityViolationException ex) {
            Logger.getLogger(BookingMealBootstraper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
