/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.bootstrapers;

import eapli.ecafeteria.application.pos.RegisterCommentController;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.CommentRepository;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.actions.Action;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.Date;
import java.util.logging.Logger;

/**
 *
 * @author Helder
 */
public class CommentBootsStrapper implements Action {

    @Override
    public boolean execute() {
        final CommentRepository commentRepo = PersistenceContext.repositories().comments();
        final MealRepository mealsRepo = PersistenceContext.repositories().meals();
        final CafeteriaUserRepository userRepo=PersistenceContext.repositories().cafeteriaUsers(false);
        Date date = new Date(1, 5, 2015);
        CafeteriaUser user=userRepo.first();
        Meal meal=mealsRepo.first();
      
        String st="ReclamacaoTopa";
        register(user, meal, st);
        
        return true;
    }

    //
    private void register(CafeteriaUser user, Meal meal, String description) {
        final RegisterCommentController controller = new RegisterCommentController();
//        try {
//            controller.registerComment(user, meal, description);
//        } catch (final DataIntegrityViolationException | DataConcurrencyException e) {
//            // ignoring exception. assuming it is just a primary key violation
//            // due to the tentative of inserting a duplicated user
//            Logger.getLogger(ECafeteriaBootstraper.class.getSimpleName())
//                    .info("EAPLI-DI001: bootstrapping existing record");
//        }
    }

}
