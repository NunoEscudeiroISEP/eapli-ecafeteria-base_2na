/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.bootstrapers;

import eapli.ecafeteria.application.meals.RegisterMenuController;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.actions.Action;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

/**
 *
 * @author Daniel Correia
 */
public class MenuBootstraper implements Action {

    @Override
    public boolean execute() {

        //datas das meal do menu1
        final Date date1 = new Date(117, 5, 12);
        final Date date2 = new Date(117, 5, 13);
        final Date date3 = new Date(117, 5, 14);
        final Date date4 = new Date(117, 5, 15);
        final Date date5 = new Date(117, 5, 16);

        //datas das meal do menu2
        final Date date6 = new Date(117, 5, 19);
        final Date date7 = new Date(117, 5, 20);
        final Date date8 = new Date(117, 5, 21);
        final Date date9 = new Date(117, 5, 22);
        final Date date10 = new Date(117, 5, 23);

        final MealRepository mealRepository = PersistenceContext.repositories().meals();
        Iterable<Meal> meals = mealRepository.findAll();

        List<Meal> mealListMenu1 = new ArrayList<>();
        List<Meal> mealListMenu2 = new ArrayList<>();
        for (Meal meal : meals) {
            if (meal.date().compareTo(date5) != 1) {
                mealListMenu1.add(meal);
            } else {
                mealListMenu2.add(meal);
            }
        }
        register(date1, date5, mealListMenu1, false, true);
        register(date6, date10, mealListMenu2, true, false);
        return false;
    }

    /**
     *
     */
    private void register(Date initialDate, Date finalDate, List<Meal> mealList, final boolean stateinProgress, final boolean statePublish) {
        final RegisterMenuController controller = new RegisterMenuController();
        try {
            controller.registerMenu(initialDate, finalDate, mealList, stateinProgress, statePublish);
        } catch (final DataIntegrityViolationException | DataConcurrencyException e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
            Logger.getLogger(ECafeteriaBootstraper.class.getSimpleName())
                    .info("EAPLI-DI001: bootstrapping existing record");
        }
    }

}
