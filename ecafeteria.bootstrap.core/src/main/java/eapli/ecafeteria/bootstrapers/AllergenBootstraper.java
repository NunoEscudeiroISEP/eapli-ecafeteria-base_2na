/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */
package eapli.ecafeteria.bootstrapers;

import eapli.ecafeteria.application.meals.RegisterAllergenController;
import eapli.framework.actions.Action;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;

import java.util.logging.Logger;


/**
 * @author 1141045@isep.ipp.pt
 * @author 1090956@isep.ipp.pt
 */
public class AllergenBootstraper implements Action {

    @Override
    public boolean execute() {
        register("Lagosta");
        register("Camarão");
        register("Amendoins");
        register("Nozes");
        register("Pistácios");
        return false;
    }

    /**
     *
     */
    private void register(String name) {
        final RegisterAllergenController controller = new RegisterAllergenController();
        try {
            controller.registerDish(name);
        } catch (final DataIntegrityViolationException | DataConcurrencyException e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
            Logger.getLogger(ECafeteriaBootstraper.class.getSimpleName())
                    .info("EAPLI-DI001: bootstrapping existing record");
        }
    }
}
