package eapli.util;

import java.util.Random;

/**
 * Created by vascopinho on 25/05/17.
 */
public class PasswordGenerator {
    public static String generate(){
        Random random = new Random();

        String upper = Strings.randomString(2, "ABCDEFGHIJAKLMNOPQRSTUVWXYZ");
        String lower = Strings.randomString(5, "abcdefghijklmnopqrstuvwxyz");
        String number = Strings.randomString(2, "1234567890");

        String password = upper + lower + number;

        char a[] = password.toCharArray();

        // Scramble the letters using the standard Fisher-Yates shuffle,
        for( int i=0 ; i<a.length ; i++ )
        {
            int j = random.nextInt(a.length);
            // Swap letters
            char temp = a[i]; a[i] = a[j];  a[j] = temp;
        }

        return new String( a );

    }
}
