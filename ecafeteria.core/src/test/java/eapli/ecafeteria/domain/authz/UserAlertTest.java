package eapli.ecafeteria.domain.authz;

import org.junit.Test;

/**
 * Created by vascopinho on 18/05/17.
 */
public class UserAlertTest {

    @Test(expected = IllegalArgumentException.class)
    public void testConstructorWithZero(){
        new UserAlert(0);
    }


    @Test(expected = IllegalArgumentException.class)
    public void testConstructorWithNegative(){
        new UserAlert(-1);
    }

}