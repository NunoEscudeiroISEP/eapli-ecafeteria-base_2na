package eapli.ecafeteria.domain.meals;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * @author 1141045@isep.ipp.pt
 * @author 1090956@isep.ipp.pt
 */
public class AllergenTest {


    @Test(expected = IllegalStateException.class)
    public void testAllergenMustNotBeNull() {
        System.out.println("must have a Allergen name");
        Allergen instance = new Allergen(null);
    }

    @Test(expected = IllegalStateException.class)
    public void testDishTypeMustNotBeEmpty() {
        System.out.println("must have a Allergen name");
        Allergen instance = new Allergen("");
    }

    @Test
    public void testEqualAllergen() {

        Allergen instance1 = new Allergen("Camaraco");
        Allergen instance1a = new Allergen("Camaraco");
        Allergen instance2 = new Allergen("Noz");

        System.out.println("must be equals");
        assertEquals(instance1, instance1a);

        System.out.println("must not be equals");
        assertNotEquals(instance1, instance2);
    }
}