/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.ecafeteria.domain.kitchen.Quantity;
import eapli.framework.domain.Designation;
import eapli.framework.domain.Money;
import java.util.Date;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Daniel Correia
 */
public class MealTest {
    
    private MealType mealType;
    private Dish dish;
    private Date date;
    private Quantity qtd;
    
    public MealTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        Money price = Money.euros(6);
        Designation name = Designation.valueOf("Arroz de Frango");
        DishType dishType= new DishType("Carne","Carne");
        dish = new Dish(dishType, name, price);
        mealType = new MealType("D", "Dinner");
        date= new Date(2017, 5, 1); 
        qtd=new Quantity(10,10,10);
        
    }
    
    @Test(expected = IllegalStateException.class)
    public void testDateMustNotBeNull() {
        System.out.println("must have an Date");
        Meal instance = new Meal(null, dish, mealType, qtd);
    }
    
    @Test(expected = IllegalStateException.class)
    public void testDishMustNotBeNull() {
        System.out.println("must have an Dish");
        Meal instance = new Meal(date, null, mealType, qtd);
    }
    
    @Test(expected = IllegalStateException.class)
    public void testMealTypeMustNotBeNull() {
        System.out.println("must have an Meal type");
        Meal instance = new Meal(date, dish, null, qtd);
    }
            
}
