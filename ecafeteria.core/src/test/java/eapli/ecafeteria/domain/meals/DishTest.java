/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eapli.ecafeteria.domain.meals;

import eapli.framework.domain.Designation;
import eapli.framework.domain.Money;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 *
 * @author AntónioRocha
 */
public class DishTest {

    private final Designation prego = Designation.valueOf("Prego");
    private DishType peixe;
    private NutricionalInfo aNutricionalInfo;

    public DishTest() {
    }

    @Before
    public void setUp() {
        peixe = new DishType("Peixe", "Peixe");
        aNutricionalInfo = new NutricionalInfo(10, 11);
    }

    @Test(expected = IllegalStateException.class)
    public void testDishTypeMustNotBeNull() {
        System.out.println("must have a Dish type");
        Dish instance = new Dish(null, prego, aNutricionalInfo, Money.euros(1));
    }

    @Test(expected = IllegalStateException.class)
    public void testNameMustNotBeNull() {
        System.out.println("must have an name");
        Dish instance = new Dish(peixe, null, aNutricionalInfo, Money.euros(5));
    }

    @Test(expected = IllegalStateException.class)
    public void testNutricionalInfoMustNotBeNull() {
        System.out.println("must have an Nutricional Info");
        Dish instance = new Dish(peixe, prego, null, Money.euros(7));
    }

    /**
     * Test of is method, of class Dish.
     */
    @Test
    public void testIs() {
        System.out.println("Attest 'is' method - Normal Behaviour");
        Dish instance = new Dish(peixe, prego, aNutricionalInfo, Money.euros(5));
        boolean expResult = true;
        boolean result = instance.is(prego);
        assertEquals(expResult, result);
    }

    /**
     * Test of changeNutricionalInfoTo method, of class Dish.
     *
     * PRP - 29.mar.2017
     */
    @Test(expected = IllegalArgumentException.class)
    public void ensureCannotChangeNutricionalInfoToNull() {
        System.out.println("ChangeNutricionalInfoTo -New nutricional info must not be null");

        final Dish Dishinstance = new Dish(peixe, prego, new NutricionalInfo(1, 1),
                Money.euros(7));
        Dishinstance.changeNutricionalInfoTo(null);
    }

    /**
     * Tests of changePriceTo method, of class Dish.
     *
     * PRP - 29.mar.2017
     */
    @Test(expected = IllegalArgumentException.class)
    public void ensureCannotChangePriceToNull() {
        System.out.println("ChangePriceTo -New price info must not be null");

        final Dish Dishinstance = new Dish(peixe, prego, new NutricionalInfo(1, 1),
                Money.euros(7));
        Dishinstance.changePriceTo(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureCannotChangePriceToNegative() {
        System.out.println("ChangePriceTo -New price can nt be negativel");

        final Dish Dishinstance = new Dish(peixe, prego, new NutricionalInfo(1, 1),
                Money.euros(1));
        Dishinstance.changePriceTo(Money.euros(-1));
    }

    @Test
    public void allergenTest() {

        Allergen camarao = new Allergen("Camarão");
        Allergen amendoins = new Allergen("Amendoins");
        Allergen nozes = new Allergen("Nozes");

        System.out.println("Dish without Allergenics");
        final Dish Dishinstance = new Dish(peixe, prego, new NutricionalInfo(1, 1), Money.euros(1));
        assertEquals(0, Dishinstance.getNumberOfAllergens());


        System.out.println("Dish with Allergenics");
        assertTrue(Dishinstance.addAllergen(camarao));
        assertTrue(Dishinstance.addAllergen(amendoins));
        assertEquals(2, Dishinstance.getNumberOfAllergens());

        System.out.println("Dish has Allergenic");
        assertTrue(Dishinstance.hasAllergen(camarao));
        assertTrue(Dishinstance.hasAllergen(amendoins));
        assertFalse(Dishinstance.hasAllergen(nozes));

        System.out.println("remove allergen from Dish");
        assertTrue(Dishinstance.removeAllergen(camarao));
        assertEquals(1, Dishinstance.getNumberOfAllergens());


        System.out.println("Dish with all Allergenics removed");
        assertTrue(Dishinstance.removeAllergen(amendoins));
        assertEquals(0, Dishinstance.getNumberOfAllergens());


        System.out.println("No allergen to remove from Dish");
        assertEquals(false, Dishinstance.removeAllergen(camarao));
        assertEquals(false, Dishinstance.removeAllergen(camarao));

    }
}
