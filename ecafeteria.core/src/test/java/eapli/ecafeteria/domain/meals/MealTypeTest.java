/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Daniel Correia
 */
public class MealTypeTest {
    
    public MealTypeTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void ensureDishTypeHasAnAcronymAndDescription() {
        new MealType("D", "Dinner");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAcronymMustNotBeEmpty() {
        System.out.println("must have non-empty acronym");
        new MealType("", "Dinner");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAcronymMustNotBeNull() {
        System.out.println("must have an acronym");
        new MealType(null, "Dinner");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDescriptionMustNotBeEmpty() {
        System.out.println("must have non-empty description");
        new MealType("D", "");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDescriptionMustNotBeNull() {
        System.out.println("must have a description");
        new MealType("D", null);
    }

    /**
     * Test of changeDescriptionTo method, of class DishType.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testchangeDescriptionToMustNotBeNull() {
        System.out.println("ChangeDescriptionTo -New description must not be null");
        final MealType instance = new MealType("D", "Dinner");
        instance.changeDescriptionTo(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testchangeDescriptionToMustNotBeEmpty() {
        System.out.println("ChangeDescriptionTo -New description must not be empty");
        final MealType instance = new MealType("D", "Dinner");
        instance.changeDescriptionTo("");
    }

    /**
     * Test of changeDescriptionTo method, of class MealType.
     */
    @Test
    public void testChangeDescriptionTo() {
        System.out.println("changeDescriptionTo");
        String newDescription = "Lunch";
        MealType instance = new MealType("D", "Dinner");
        instance.changeDescriptionTo(newDescription);
        final String expResult = newDescription;
        final String result = instance.description();
        assertEquals(expResult, result);
    }

    /**
     * Test of is method, of class MealType.
     */
    @Test
    public void testIs() {
        System.out.println("Attest is method");
        final String id = "D";
        final MealType instance = new MealType(id, "Dinner");
        final boolean expResult = true;
        final boolean result = instance.is(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of id method, of class MealType.
     */
    @Test
    public void testId() {
        System.out.println("Attest id method");
        final String acronym = "D";
        final MealType instance = new MealType(acronym, "Dinner");
        final boolean expResult = true;
        final boolean result = acronym.equals(instance.id());
        assertEquals(expResult, result);
    }

}
