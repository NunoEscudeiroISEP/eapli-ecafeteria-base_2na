/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.ecafeteria.domain.kitchen.Quantity;
import eapli.framework.domain.Designation;
import eapli.framework.domain.Money;
import java.util.Date;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Daniel Correia
 */
public class MenuTest {
    
    private Date initialDate;
    private Date finalDate;
    private Menu instance;
    private Quantity qtd;
    
    
    public MenuTest() {
    }

    
    @Before
    public void setUp() {
      this.initialDate = new Date(2017, 5, 15);
      this.finalDate = new Date(2017, 5, 19);
      this.instance = new Menu(initialDate, finalDate);
    }
   
    /**
     * Test of addMeal method, of class Menu.
     */
    @Test
    public void testAddMeal() {
        System.out.println("addMeal");
        MealType mealType= new MealType("D","Dinner");
        Date mealDate = new Date(2017,5,15);
        DishType dishType = new DishType("peixe", "prato peixe");
        Dish dish = new Dish(dishType, Designation.valueOf("Sardinhas assadas"),Money.euros(3));
        Meal meal = new Meal(mealDate, dish, mealType, qtd);
        boolean expResult = true;
        boolean result = instance.addMeal(meal);
        assertEquals(expResult, result);
    }

    /**
     * Test of period method, of class Menu.
     */
    @Test
    public void testPeriod() {
        System.out.println("period");
        long expResult = 4;
        long result = instance.period();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of validateMealDate method, of class Menu.
     */
    @Test
    public void testValidateMealDate() {
        System.out.println("validateMealDate");
        Date mealDate = new Date(2017,5,16);
        boolean expResult = true;
        boolean result = instance.validateMealDate(mealDate);
        assertEquals(expResult, result);
    }

    /**
     * Test of validateDates method, of class Menu.
     */
    @Test
    public void testValidateDates() {
        System.out.println("validateDates");
        boolean expResult = true;
        boolean result = instance.validateDates(initialDate, finalDate);
        assertEquals(expResult, result);
    }

    
}
