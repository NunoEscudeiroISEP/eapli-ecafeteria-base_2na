package eapli.ecafeteria.domain.kitchen;

import org.junit.Test;

/**
 * Created by vascopinho on 18/05/17.
 */
public class KitchenAlertTest {

    @Test(expected = IllegalArgumentException.class)
    public void testRedSmallerThanYellow() throws Exception {
        new KitchenAlert(10,80);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBiggerThan100() throws Exception{
        new KitchenAlert(90,123123);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSmallerThan0() throws Exception{
        new KitchenAlert(-1,75);
    }

}