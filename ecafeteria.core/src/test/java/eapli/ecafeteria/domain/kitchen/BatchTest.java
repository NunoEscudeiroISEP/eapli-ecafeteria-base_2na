/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */
package eapli.ecafeteria.domain.kitchen;

import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Daniel
 */
public class BatchTest {

    public BatchTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }



    @Test
    public void testBatchNumberTo() {
        System.out.println("attest changeBatchNumberTo");
        final Batch instance = new Batch("0", new Date());
        final String newBatchNumber = "2";
        instance.setBatchnumber(newBatchNumber);
        final String expResult = newBatchNumber;
        final String result = instance.getBatchnumber();
        assertEquals(expResult, result);
    }

    /**
     * Test of batch number method, of class Batch.
     */
    @Test
    public void testBatchNumber() {
        System.out.println("batch");
        final String batch = "0";
        final Batch instance = new Batch("0",new Date());
        final boolean expResult = true;
        boolean result = false;
        if (batch == instance.getBatchnumber()) result = true;
        assertEquals(expResult, result);
    }

}
