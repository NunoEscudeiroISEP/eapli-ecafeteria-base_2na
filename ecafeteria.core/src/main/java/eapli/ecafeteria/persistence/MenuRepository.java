/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.framework.persistence.repositories.DataRepository;
import java.util.Date;
import javax.swing.text.StyledEditorKit;
import eapli.ecafeteria.domain.meals.Menu;


/**
 *
 * @author Daniel Correia
 */
public interface MenuRepository extends DataRepository<Menu, Long> {
    
    Menu findByDate(Date initialDate, Date finalDate);
    
    Menu findByDay(Date day);
    
    Iterable<Menu> findAllPublished();
    
    Iterable<Menu> findAllInProgress();
    
}
