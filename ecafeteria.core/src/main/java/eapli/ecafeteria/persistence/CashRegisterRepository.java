
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;
import eapli.ecafeteria.domain.pos.CashRegister;
import eapli.framework.persistence.repositories.DataRepository;
/**
 *
 * @author CarlosPinho (1980380)
 */
public interface CashRegisterRepository extends DataRepository<CashRegister, Long>{
  
    CashRegister findByAcronym(String acronym);
 
    Iterable<CashRegister> closedCashRegisters();
}