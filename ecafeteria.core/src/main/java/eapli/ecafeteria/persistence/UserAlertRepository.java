package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.authz.UserAlert;
import eapli.framework.persistence.repositories.DataRepository;

/**
 * Created by vascopinho on 18/05/17.
 */
public interface UserAlertRepository extends DataRepository<UserAlert, Long> {
    UserAlert getUserAlert();
}
