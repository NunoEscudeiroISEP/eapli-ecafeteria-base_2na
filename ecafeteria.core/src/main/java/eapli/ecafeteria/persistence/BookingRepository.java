/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.meals.Booking;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.persistence.repositories.DataRepository;

/**
 *
 * @author marci
 */
public interface BookingRepository extends DataRepository<Booking, Long> {
    
  /**
   * Return all bookins of a user. 
   * @param user - The user.
   * @return A colloction of bookings.
   */
  Iterable<Booking> getAllBookingsFromUser(CafeteriaUser user);
  boolean ChangeBookingStatusToDelivered(Booking booking);
  boolean ChangeBookingStatusToCAncelled(Booking booking);
}
