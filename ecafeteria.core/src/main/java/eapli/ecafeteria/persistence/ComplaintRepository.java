/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;


import eapli.ecafeteria.domain.pos.Complaint;
import eapli.framework.domain.Designation;
import eapli.framework.persistence.repositories.DataRepository;

/**
 *
 * @author Topa
 */
public interface ComplaintRepository extends DataRepository<Complaint, Long>{
    /**
     * return comment 
     * @param name
     * @return 
     */
    Complaint getCommentByName(String name);
    Complaint getCommentById(Long id);
    
}
