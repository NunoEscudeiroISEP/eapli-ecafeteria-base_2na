/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.pos.Comment;
import eapli.framework.persistence.repositories.DataRepository;

/**
 *
 * @author Topa
 */
public interface CommentRepository extends DataRepository<Comment, Long> {
    
    /**
     * return comment 
     * @param name
     * @return 
     */
    Comment getCommentByName(String name);
   
    
}
