package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.kitchen.KitchenAlert;
import eapli.framework.persistence.repositories.DataRepository;

/**
 * Created by vascopinho on 18/05/17.
 */
public interface KitchenAlertRepository extends DataRepository<KitchenAlert, Long> {

    KitchenAlert getKitchenAlert();
}
