/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.cafeteria;

import eapli.framework.domain.Money;
import eapli.framework.domain.ValueObject;
import java.util.Currency;
import javax.persistence.Embeddable;

/**
 *
 * @author marcio
 */
@Embeddable
public class Balance implements ValueObject{
    
    private Money money;
    
    protected Balance() {
        // for ORM tool only
    }
    
    public Balance(double ammount, Currency currency){
        this.money = new Money(ammount, currency);
    }
    
    public void addMoney(Money money){
        this.money = this.money.add(money);
    }
    
    public boolean canPay(Money amount) {
        return this.money.greaterThanOrEqual(amount);
    }

    public boolean Pay(Money money) {
        if(this.money.subtract(money).amount() < 0)
            return false;
        this.money = this.money.subtract(money);
        return true;
    }
    
    public Money money(){
        return this.money;
    }
    
    
}
