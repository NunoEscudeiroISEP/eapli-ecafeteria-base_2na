/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.pos;

import javax.persistence.Embeddable;

/**
 *
 * @author Topa
 */
@Embeddable
public class BookingStatus {
    
    
     public enum Status {
        BOOKED, CANCELLED, DELIVERED;
    }

    private Status status;

    public BookingStatus() {
        this.status = Status.BOOKED;
    }
    
    public boolean changeToDelivered(){
        if(status==Status.BOOKED){
            this.status=Status.DELIVERED;
            return true;
        }
            else return false;
    }
    
     public boolean changeToCancelled(){
        if(status==Status.BOOKED ){
            this.status=Status.CANCELLED;
            return true;
        }
            else return false;
    }
     
   
    
}
