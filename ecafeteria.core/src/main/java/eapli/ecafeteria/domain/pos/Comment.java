/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.pos;

import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.framework.domain.AggregateRoot;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;

/**
 *
 * @author Helder
 */
@Entity
public class Comment extends Feedback implements AggregateRoot<String>, Serializable{

     private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long pk;
    @Version
    private Long version;

    @Column(unique = true, name="name")
    private String commentDescription;

    private boolean active;
    @ManyToOne
    private CafeteriaUser commentUser;
    @ManyToOne
    private Meal commentMeal;

    protected Comment() {
        // for ORM
    }

    // Apenas para prova de conceito
    public Comment(String description) {
        this.commentDescription = description;
        this.active = true;
    }

    public Comment(CafeteriaUser user, Meal meal, String description) {
        if (user == null || meal == null || description == null) {
            throw new IllegalStateException("All fields are mandatory");
        }
        this.commentUser = user;
        this.commentMeal = meal;
        this.commentDescription = description;
        this.active = true;
    }

    public Comment(CafeteriaUser user, String description) {
        if (user == null || description == null) {
            throw new IllegalStateException("All fields are mandatory");
        }
        this.commentUser = user;
        this.commentDescription = description;
        this.active = true;
    }

    @Override
    public int hashCode() {
        return this.commentDescription.hashCode() + this.commentUser.hashCode();
    }

    public boolean toogleState() {
        this.active = !this.active;
        return isActive();
    }

    public boolean isActive() {
        return this.active;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the pk fields are not set
        if (!(object instanceof Comment)) {
            return false;
        }
        Comment other = (Comment) object;
        if ((this.pk == null && other.pk != null) || (this.pk != null && !this.pk.equals(other.pk))) {
            return false;
        }
        return true;
    }

   

    @Override
    public String toString() {
        return this.commentDescription;
    }

    @Override
    public boolean sameAs(Object other) {
        final Comment comment = (Comment) other;
        return this.equals(comment) && feedBackDescription().equals(comment.feedBackDescription())
                && isActive() == comment.isActive();
    }

    @Override
    public boolean is(String id) {
        return id.equalsIgnoreCase(this.commentDescription);
    }

    @Override
    public String id() {
        return this.commentDescription;
    }

    @Override
    public String feedBackDescription() {
         return this.commentDescription;
    }

    @Override
    public Meal feedBacktMeal() {
       return this.commentMeal;
    }

    @Override
    public Long longPk() {
        return this.pk;
    }
    
   

}
