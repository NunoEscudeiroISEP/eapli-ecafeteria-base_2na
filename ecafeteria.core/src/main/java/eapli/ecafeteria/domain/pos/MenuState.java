/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.pos;
/**
 *
 * @author Daniel Correia
 */
public interface MenuState {
    
    
    void changeStateToInProgress();
    void changeStateToPublished();
    boolean isInProgress();
    boolean isPublished();
    
}
