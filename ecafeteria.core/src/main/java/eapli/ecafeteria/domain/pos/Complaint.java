/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.pos;

import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.framework.domain.AggregateRoot;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;

/**
 *
 * @author Helder
 */
@Entity
public class Complaint extends Feedback implements AggregateRoot<String>, Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long pk;
    @Version
    private Long version;

    @Column(unique = true, name = "name")
    private String complaintDescription;

    private boolean active;
    @ManyToOne
    private CafeteriaUser complaintUser;
    @ManyToOne
    private Meal complaintMeal;

    protected Complaint() {
        // for ORM
    }

    // Apenas para prova de conceito
    public Complaint(String description) {
        this.complaintDescription = description;
        this.active = true;
    }

    public Complaint(CafeteriaUser user, Meal meal, String description) {
        if (user == null || meal == null || description == null) {
            throw new IllegalStateException("All fields are mandatory");
        }
        this.complaintUser = user;
        this.complaintMeal = meal;
        this.complaintDescription = description;
        this.active = true;
    }

    public Complaint(CafeteriaUser user, String description) {
        if (user == null || description == null) {
            throw new IllegalStateException("All fields are mandatory");
        }
        this.complaintUser = user;
        this.complaintDescription = description;
        this.active = true;
    }

    @Override
    public int hashCode() {
        return this.complaintDescription.hashCode() + this.complaintUser.hashCode();
    }

    public boolean toogleState() {
        this.active = !this.active;
        return isActive();
    }

    public boolean isActive() {
        return this.active;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the pk fields are not set
        if (!(object instanceof Complaint)) {
            return false;
        }
        Complaint other = (Complaint) object;
        if ((this.pk == null && other.pk != null) || (this.pk != null && !this.pk.equals(other.pk))) {
            return false;
        }
        return true;
    }

    @Override
    public String feedBackDescription() {
        return this.complaintDescription;
    }

    @Override
    public String toString() {
        return this.complaintDescription;
    }

    @Override
    public boolean sameAs(Object other) {
        final Complaint complaint = (Complaint) other;
        return this.equals(complaint) && feedBackDescription().equals(complaint.feedBackDescription())
                && isActive() == complaint.isActive();
    }

    @Override
    public boolean is(String id) {
        return id.equalsIgnoreCase(this.complaintDescription);
    }

    @Override
    public String id() {
        return this.complaintDescription;
    }

    @Override
    public Meal feedBacktMeal() {
        return this.complaintMeal;
    }

    @Override
    public Long longPk() {
        return this.longPk();
    }

}
