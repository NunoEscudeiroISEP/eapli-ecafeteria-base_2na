/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.pos;

import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.authz.Username;
import eapli.framework.domain.AggregateRoot;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.Version;

/**
 *
 * @author Carlos Pinho & Jorge Esteves
 */
@Entity
public class Shift implements AggregateRoot<Long>, Serializable {

    private static final long serialVersionUID = 1L;

    //ORM primary key
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long pk;
    @Version
    private Long version;

    @ManyToOne
    private CashRegister cashRegister;
    @ManyToOne
    private SystemUser user;
    
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date closeDate;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date openDate;
    
    protected Shift() {
        //for ORM
    }

    public Shift(Date open, CashRegister cash, SystemUser user) {
        if (open == null || cash == null || user == null) {
            throw new IllegalArgumentException("Cannot be null");
        }
            this.openDate = open;
            this.cashRegister = cash;
            this.user = user;
            this.closeDate = null;
    }

    public void closeShiftSession(Date closeTime) {
        this.closeDate = closeTime;
    }

    @Override
    public boolean sameAs(Object other) {
        final Shift session = (Shift) other;

        return this.equals(session) 
                && this.cashRegister.equals(session.cashRegister)
                && this.openDate.equals(session.openDate)
                && this.user.equals(session.user)
                && this.closeDate.equals(session.closeDate);
    }

    @Override
    public boolean is(Long id) {
        return id.equals(this.pk);
    }

    @Override
    public Long id() {
        return this.pk;
    }
    
    public Date openDate(){
        return this.openDate;
    }
    
    @Override
    public int hashCode() {
        return this.pk.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Shift)) {
            return false;
        }
        final Shift other = (Shift) o;
        return id().equals(other.id());
    }

}
