/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.pos.exceptions;

/**
 *
 * @author Topa
 */
public class BookingDeliveringException extends RuntimeException {
    
    public BookingDeliveringException(String message) {
        super(message);
    }
    
}
