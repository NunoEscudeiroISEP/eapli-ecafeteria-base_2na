/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.pos;

import eapli.ecafeteria.domain.meals.Meal;

/**
 *
 * @author Helder
 */
public abstract class Feedback {

    public abstract int hashCode();

    public abstract boolean toogleState();

    public abstract boolean isActive();

    public abstract boolean equals(Object object);

    public abstract String toString();

    public abstract boolean sameAs(Object other);

    public abstract boolean is(String id);

    public abstract String feedBackDescription();

    public abstract Meal feedBacktMeal();
    
    public abstract Long longPk();

}
