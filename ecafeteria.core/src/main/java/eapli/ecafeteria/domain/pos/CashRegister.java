package eapli.ecafeteria.domain.pos;

import eapli.ecafeteria.domain.cafeteria.OrganicUnit;
import eapli.framework.domain.AggregateRoot;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;

/**
 *
 * @author Jorge Esteves <1150742@isep.ipp.pt>
 */
@Entity
public class CashRegister implements AggregateRoot<String>, Serializable {

    private static final long serialVenrionUID = 1L;
    @Id
    @GeneratedValue
    private Long pk;
    @Version
    private Long version;

    @Column(unique = true)
    private String acronym;
    private String name;
    private String description;
    private boolean open;
    @ManyToOne
    private OrganicUnit organicUnit;

    protected CashRegister() {
// for ORM
    }

    public CashRegister(String acronym, String name, String description, OrganicUnit orga) {
        if (acronym == null || name == null || acronym.trim().isEmpty()) {
            throw new IllegalStateException();
        }
        this.acronym = acronym;
        this.name = name;
        this.description = description;
        this.organicUnit = orga;
        this.open = false;
    }

    public CashRegister(String acronym, String name, String description) {
        if (acronym == null || name == null || acronym.trim().isEmpty()) {
            throw new IllegalStateException();
        }
        this.acronym = acronym;
        this.name = name;
        this.description = description;
        this.organicUnit = null;
        this.open = false;
    }

    public String id() {
        return this.acronym;
    }

    @Override
    public boolean sameAs(Object other) {
        if (!(other instanceof CashRegister)) {
            return false;
        }
        final CashRegister that = (CashRegister) other;
        if (this == that) {
            return true;
        }
        return this.acronym.equals(that.acronym)
                && name.equals(that.name)
                && organicUnit.equals(that.organicUnit);
    }

    public boolean isOpen() {
        return this.open;
    }

    public String name() {
        return this.name;
    }

    public OrganicUnit organic() {
        return this.organicUnit;
    }

    public boolean toogleState() {
        this.open = !this.open;

        return isOpen();

    }

    @Override

    public boolean is(String acronym) {
        return acronym.equalsIgnoreCase(this.acronym);
    }

    @Override

    public int hashCode() {
        return this.acronym.hashCode();
    }

    @Override

    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }
        if (!(o instanceof CashRegister)) {
            return false;
        }
        final CashRegister other = (CashRegister) o;
        return this.id().equals(other.id());
    }

}
