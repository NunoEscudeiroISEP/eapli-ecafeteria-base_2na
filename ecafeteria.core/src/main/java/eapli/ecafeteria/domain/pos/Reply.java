/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.pos;

import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.meals.Meal;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Version;

/**
 *
 * @author Helder
 */
public class Reply{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long pk;
    @Version
    private Long version;

    @Column(unique = true, name="name")
    private String replyDescription;

    private boolean active;
    @ManyToOne
    private CafeteriaUser commentUser;
    @ManyToOne
    private Meal commentMeal;
    @OneToOne
    private Comment comment;

    public Reply() {
        //for ORM
    }

    public Reply(String description, Comment comment) {
        this.replyDescription = description;        
        this.comment=comment;
    }

    

}
