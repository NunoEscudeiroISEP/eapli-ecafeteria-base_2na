package eapli.ecafeteria.domain.authz;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;

/**
 * Created by vascopinho on 18/05/17.
 */

@Entity
public class UserAlert implements Serializable {
    @Id
    @GeneratedValue
    private Long pk;

    private int userLimit;

    @Temporal(TemporalType.TIMESTAMP)
    private Calendar insertionDate;

    public UserAlert(final int limit){
        if (limit <= 0){
            throw new IllegalArgumentException("Limit should be bigger than 0");
        }
        this.userLimit = limit;
        this.insertionDate = Calendar.getInstance();
    }

    protected UserAlert() {
        // for ORM only
    }


    public int getLimit(){
        return this.userLimit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserAlert)) {
            return false;
        }

        final UserAlert other = (UserAlert) o;
        return getLimit() == other.getLimit();
    }

    @Override
    public int hashCode() {
        int result = pk != null ? pk.hashCode() : 0;
        result = 31 * result + userLimit;
        result = 31 * result + (insertionDate != null ? insertionDate.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "UserAlert{" +
                "limit=" + userLimit +
                '}';
    }
}
