package eapli.ecafeteria.domain.meals;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;


/**
 * @author 1141045@isep.ipp.pt
 * @author 1090956@isep.ipp.pt
 */
@Entity
public class Allergen implements Serializable {

    @Id
    @GeneratedValue
    private Long pk;

    private String name;

    public Allergen(final String name){
        if (name == null || name.length() == 0) {
            throw new IllegalStateException();
        }
        this.name = name;
    }

    protected Allergen() {
        // for ORM only
    }


    public String getName(){
        return this.name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Allergen)) {
            return false;
        }

        final Allergen other = (Allergen) o;
        return getName().equals(other.getName());
    }

    @Override
    public int hashCode() {
        return this.name.hashCode();
    }

}
