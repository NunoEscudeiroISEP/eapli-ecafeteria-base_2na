/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.ecafeteria.domain.kitchen.Quantity;
import eapli.ecafeteria.domain.pos.MenuState;
import eapli.ecafeteria.domain.meals.Meal;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.Version;

/**
 *
 * @author Daniel Correia
 */
@Entity
public class Menu implements MenuState, Serializable, Cloneable {

    private static final long serialVersionUID = 1L;

    @Version
    private Long version;

    @Id
    @GeneratedValue
    private Long pk;

    @Temporal(javax.persistence.TemporalType.DATE)
    private Date initialDate;

    @Temporal(javax.persistence.TemporalType.DATE)
    private Date finalDate;

    @OneToMany(cascade = CascadeType.MERGE)
    private List<Meal> MealList;

    @Column(nullable = true)
    private boolean stateinProgress;

    @Column(nullable = false)
    private boolean statePublish;

    public Menu() {
        // for ORM only
    }

    public Menu(final Date initialDate, final Date finalDate) {
        if (initialDate == null || finalDate == null) {
            throw new IllegalStateException();
        }

        this.initialDate = initialDate;
        this.finalDate = finalDate;

    }

    public Menu(final Date initialDate, final Date finalDate, final List<Meal> mealList, final boolean stateinProgress, final boolean statePublish) {
        if (initialDate == null || finalDate == null || mealList == null) {
            throw new IllegalStateException();
        }

        this.initialDate = initialDate;
        this.finalDate = finalDate;
        this.MealList = mealList;
        this.stateinProgress = stateinProgress;
        this.statePublish = statePublish;

    }

    public boolean addMeal(Meal meal) {
        if (MealList == null) {
            MealList = new ArrayList<Meal>();
        }
        if (validateMealDate(meal.date())) {
            MealList.add(meal);
            return true;
        }
        return false;
    }
    
    public void removeMeal(Meal meal){
        MealList.remove(meal);
    }

    public long period() {
        return ((finalDate.getTime() - initialDate.getTime() + 3600000L) / 86400000L);
    }

    public boolean validateMealDate(Date mealDate) {
        if (this.initialDate.compareTo(mealDate) != 1
                && this.finalDate.compareTo(mealDate) != -1) {
            return true;
        } else {
            System.out.println("Meal Date is not valid.");
            return false;
        }
    }

    public boolean validateDates(Date initialDate, Date finalDate) {
        if (finalDate.compareTo(initialDate) != -1) {
            return true;
        } else {
            System.out.println("Dates are not valid.");
            return false;
        }
    }

    public List<Meal> meals() {
        return MealList;
    }

    public Date initialDate() {
        return this.initialDate;
    }
    
    public void setInitialDate(Date initialDate){
        this.initialDate=initialDate;
    }

    public Date finalDate() {
        return this.finalDate;
    }
    
    public void setFinalDate(Date finalDate){
        this.finalDate=finalDate;
    }

    @Override
    public boolean isInProgress() {
        return stateinProgress;
    }

    @Override
    public boolean isPublished() {
        return statePublish;
    }

    @Override
    public void changeStateToInProgress() {
        this.stateinProgress = true;
        this.statePublish = false;
    }

    @Override
    public void changeStateToPublished() {
        this.stateinProgress = false;
        this.statePublish = true;
    }

    
    @Override
    public Menu clone() throws CloneNotSupportedException  {
        Menu menu_clone = new Menu();
        menu_clone.initialDate = new Date(this.initialDate.getYear(), initialDate.getMonth(), initialDate.getDate());
        menu_clone.finalDate = new Date(this.finalDate.getYear(), finalDate.getMonth(), finalDate.getDate());
        for (Meal meal : MealList) {
            menu_clone.addMeal(new Meal(meal.date(), meal.dish(), meal.mealType(), meal.Quantidade()));
        }
        menu_clone.stateinProgress=this.stateinProgress;
        menu_clone.statePublish = this.statePublish;
        return menu_clone;
    }
}
