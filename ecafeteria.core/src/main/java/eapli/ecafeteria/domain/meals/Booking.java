/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.pos.BookingStatus;
import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Version;

/**
 *
 * @author marci
 */
@Entity
public class Booking implements Serializable {

    private static final long serialVersionUID = 1L;

    // ORM primary key
    @Id
    @GeneratedValue
    private Long pk;
    @Version
    private Long version;

    @OneToOne(cascade = CascadeType.ALL)
    private Meal meal;
    @OneToOne(cascade = CascadeType.ALL)
    private CafeteriaUser customer;
    @Embedded
    private BookingStatus bookingStatus;

    protected Booking() {
        // for ORM
    }

    public Booking(Meal meal, CafeteriaUser customer ) {
        if (meal == null || customer == null) {
            throw new IllegalStateException();
        }
        this.meal = meal;
        this.customer = customer;
        this.bookingStatus=new BookingStatus();
    }

    /**
     * Returns the meal.
     *
     * @return Meal.
     */
    public Meal getMeal() {
        return this.meal;
    }
    public BookingStatus status(){
        return this.bookingStatus;
    }

    @Override
    public String toString() {
        return "Booking{" + "meal=" + meal + ", customer=" + customer.id() + ", bookingStatus=" + bookingStatus + '}';
    }
    
    
    
}
