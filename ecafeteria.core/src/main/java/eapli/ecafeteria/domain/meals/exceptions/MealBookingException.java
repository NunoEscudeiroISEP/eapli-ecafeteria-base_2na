/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals.exceptions;


/**
 *
 * @author marcio
 */
public class MealBookingException extends RuntimeException {
    
    public MealBookingException(String message) {
        super(message);
    }
    
}
