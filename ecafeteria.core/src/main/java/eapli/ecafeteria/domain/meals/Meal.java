/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.ecafeteria.domain.kitchen.Quantity;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.Version;

/**
 * A Meal 
 * 
 * @author Daniel Correia 1131294/Daniel Lopes 1131149
 * 
 */

@Entity
// 
public class Meal implements Serializable, Cloneable  {
    
    private static final long serialVersionUID = 1L;
    
    @Version
    private Long version;
    
    @Id
    @GeneratedValue
    private Long pk;
    
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date date;
    @OneToOne(cascade = CascadeType.MERGE)
    private Dish dish;
    @ManyToOne(cascade = CascadeType.MERGE)
    private MealType mealType;
    @Embedded
    private Quantity qtd;
    
    public Meal() {
        // for ORM only
    }
    
    public Meal(final Date date, final Dish dish, final MealType mealType, Quantity qtd){
        if ( date == null || dish == null || mealType == null || qtd==null){
            throw new IllegalStateException();
        }
        
        this.date = date;
        this.dish = dish;
        this.mealType = mealType;
        this.qtd=qtd;
    }
    
     @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Meal)) {
            return false;
        }

        final Meal other = (Meal) o;
        return mealType().equals(other.mealType()) && date().equals(other.date()) && dish().equals(other.dish());
    }
    
    
    public MealType mealType(){
        return this.mealType;
    }
    
    public Date date(){
        return this.date;
    }
    
    public Dish dish(){
        return this.dish;
    }
    
    public Quantity Quantidade(){
        return this.qtd;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone(); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
