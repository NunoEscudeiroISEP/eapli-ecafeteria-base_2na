/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.kitchen;

import javax.persistence.Embeddable;

/**
 *
 * @author Helder
 */
@Embeddable
public class Quantity {
    private int forecast;
    private int sold;
    private int produced;
    
    protected Quantity(){
     // for ORM
    }
    
    public Quantity(int forecast, int sold, int produced){
     this.forecast=forecast;
     this.sold=sold;
     this.produced=produced;
    }

    /**
     * @return the forecast
     */
    public int mealsForecast() {
        return forecast;
    }

    
    /**
     * @return the solded meals
     */
    public int mealsSolded() {
        return sold;
    }

    /**
     * @param sold the sold to set
     */
    public void updateSoldedMeals(int sold) {
        this.sold = sold;
    }

    /**
     * @return the produced
     */
    public int mealsProduced() {
        return produced;
    }


    
    
}
