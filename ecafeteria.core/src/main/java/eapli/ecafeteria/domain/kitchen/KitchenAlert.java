package eapli.ecafeteria.domain.kitchen;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;

/**
 * Created by vascopinho on 18/05/17.
 */
@Entity
public class KitchenAlert implements Serializable {

    @Id
    @GeneratedValue
    private Long pk;

    private int yellow;
    private int red;

    @Temporal(TemporalType.TIMESTAMP)
    private Calendar insertionDate;

    public KitchenAlert(final int red, final int yellow){
        if (red < 0 || yellow < 0 || yellow > 100 || red > 100){
            throw new IllegalArgumentException("Numbers should be between 0 and 100");
        }
        if (red < yellow){
            throw new IllegalArgumentException("Red must be bigger ");
        }
        this.red = red;
        this.yellow = yellow;
        this.insertionDate = Calendar.getInstance();
    }

    protected KitchenAlert() {
        // for ORM only
    }


    public int getYellow(){
        return this.yellow;
    }

    public int getRed(){
        return this.red;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof KitchenAlert)) {
            return false;
        }

        final KitchenAlert other = (KitchenAlert) o;
        return getYellow() == other.getYellow() && getRed()==other.getRed();
    }
    @Override
    public int hashCode() {
        int result = pk != null ? pk.hashCode() : 0;
        result = 31 * result + yellow;
        result = 31 * result + red;
        return result;
    }

    @Override
    public String toString() {
        return "KitchenAlert{" +
                "yellow=" + yellow +
                ", red=" + red +
                '}';
    }
}
