/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.kitchen;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;

/**
 *
 * @author Daniel
 */
@Entity
public class Batch {

    @Id
    @GeneratedValue
    private Long pk;

    private String batchnumber;

    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dayUsed;

    @Temporal(javax.persistence.TemporalType.DATE)
    private Date expirationdate;

    protected Batch() {
        // for ORM
    }

    public Batch(String batchnumber, Date expirationdate) {
        this.batchnumber = batchnumber;
        this.expirationdate = expirationdate;
        this.dayUsed = new Date();
    }

    public Batch(String batchnumber) {
        this.batchnumber = batchnumber;
    }

    public Batch(Date usedDate) {
        this.dayUsed = usedDate;
    }

    /**
     * @return the batchnumber
     */
    public String getBatchnumber() {
        return batchnumber;
    }

    /**
     * @param batchnumber the batchnumber to set
     */
    public void setBatchnumber(String batchnumber) {
        this.batchnumber = batchnumber;
    }

    /**
     * @return the expirationdate
     */
    public String getExpirationdateString() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        return dateFormat.format(getExpirationdate());
    }

    /**
     * @param expirationdate the expirationdate to set
     */
    public void setExpirationdate(Date expirationdate) {
        this.expirationdate = expirationdate;
    }

    /**
     * @return the dayUsed
     */
    public String getDayUsedString() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        return dateFormat.format(getDayUsed());
    }

    /**
     * @param dayUsed the dayUsed to set
     */
    public void setDayUsed(Date dayUsed) {
        this.dayUsed = dayUsed;
    }

    /**
     * @return the dayUsed
     */
    public Date getDayUsed() {
        return dayUsed;
    }

    /**
     * @return the expirationdate
     */
    public Date getExpirationdate() {
        return expirationdate;
    }
}
