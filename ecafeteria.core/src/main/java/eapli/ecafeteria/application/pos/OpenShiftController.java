/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.pos;

import eapli.ecafeteria.domain.pos.Shift;
import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.pos.CashRegister;

import eapli.ecafeteria.persistence.CashRegisterRepository;
import eapli.ecafeteria.persistence.ShiftRepository;

import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 *
 * @author Carlos Pinho
 */
public class OpenShiftController {

    private final CashRegisterRepository cashRegisterRepository = PersistenceContext.repositories().cashRegisters();

    private final ShiftRepository shiftRepository = PersistenceContext.repositories().shifts();

    public boolean openCashRegister(Date openTime, CashRegister cashRegister)
            throws DataIntegrityViolationException, DataConcurrencyException {
        Application.ensurePermissionOfLoggedInUser(ActionRight.SALE);

        final SystemUser user = Application.session().session().authenticatedUser();
        if (cashRegister.toogleState()) {
            System.out.println("Opening session!");
            this.cashRegisterRepository.save(cashRegister);

            return true;
        }

        return false;
    }

    public Shift openShift(Date openTime, CashRegister cashRegister)
            throws DataIntegrityViolationException, DataConcurrencyException {
        Application.ensurePermissionOfLoggedInUser(ActionRight.SALE);

        final SystemUser user = Application.session().session().authenticatedUser();

        boolean have_match = false;

        for (Shift shift : shiftRepository) {

            if (shift.openDate().getDay() == openTime.getDay()
                    && shift.openDate().getMonth() == openTime.getMonth()
                    && shift.openDate().getYear() == openTime.getYear()) {
                if (shift.openDate().getHours() <= 15 && openTime.getHours() <= 15) {
                    have_match = true;
                    break;
                }

                if (shift.openDate().getHours() > 15 && openTime.getHours() > 15) {
                    have_match = true;
                    break;
                }

            }

        }

        if (!have_match) {
            System.out.println("new shift created");
            
            return this.shiftRepository.save(new Shift(openTime, cashRegister, user));
        }else
        {
         System.out.println("shift already exists in DataBase");
        }

        return null;
    }

    public Iterable<CashRegister> cashRegisters() {
        return this.cashRegisterRepository.findAll();
    }

    public Iterable<Shift> shiftS() {
        return this.shiftRepository.findAll();
    }
}
