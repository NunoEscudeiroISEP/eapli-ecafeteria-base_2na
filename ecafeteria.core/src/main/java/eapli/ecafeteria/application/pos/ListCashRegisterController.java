
package eapli.ecafeteria.application.pos;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.pos.CashRegister;
import eapli.ecafeteria.persistence.CashRegisterRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;

/**
 *
 * @author Jorge Esteves & Carlos Pinho
 */
public class ListCashRegisterController implements Controller{
    
    private final CashRegisterRepository repository = PersistenceContext.repositories().cashRegisters();

    public Iterable<CashRegister> listCashRegister() {
        Application.ensurePermissionOfLoggedInUser(ActionRight.ADMINISTER);
        return this.repository.findAll();
    }
}
