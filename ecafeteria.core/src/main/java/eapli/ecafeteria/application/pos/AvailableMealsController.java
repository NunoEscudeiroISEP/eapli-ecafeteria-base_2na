/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.pos;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.MealTypeRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

/**
 *
 * @author Helder
 */
public class AvailableMealsController implements Controller{
    
    
   private final MealRepository mealRepository = PersistenceContext.repositories().meals();
    private final MealTypeRepository mealTypeRepository=PersistenceContext.repositories().mealTypes();
    
//     public Iterable<Meal> getAvailableMealsByDate(Date date) {
//        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_DELIVERY);
//        if (date == null) {
//            throw new IllegalArgumentException();
//        }
//        // verificar se esta ppublicado num menu
//        // Perguntar ao menu quais as meals que existem para este dia
//        return mealRepository.findByDate(date);
//    }
     
     public HashMap<Meal,Integer> getAvailableMealsToBook(Date date){
         Application.ensurePermissionOfLoggedInUser(ActionRight.SALE);
                Iterable<Meal> meals=mealRepository.findByDate(date);
                HashMap<Meal,Integer> map=new HashMap<>();
                
                Iterator it=meals.iterator();
                
                
                
                while(it.hasNext()){
                    int sobram=((Meal)it).Quantidade().mealsProduced() - ((Meal)it).Quantidade().mealsSolded();
                    map.put((Meal)it,sobram);
                }
         
              return map;                
     }
     
     
     
     
     
     
    
}   
