/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.pos;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.pos.Comment;
import eapli.ecafeteria.persistence.CommentRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;

/**
 *
 * @author Topa
 */
public class RegisterCommentController implements Controller {

    private final CommentRepository repository = PersistenceContext.repositories().comments();

    public Comment registerComment(final CafeteriaUser user, Meal meal, String description) throws DataIntegrityViolationException, DataConcurrencyException {

        Application.ensurePermissionOfLoggedInUser(ActionRight.ADMINISTER);

        final Comment newComment = new Comment(user, meal, description);

        Comment ret = this.repository.save(newComment);

        return ret;
    }

}
