
package eapli.ecafeteria.application.pos;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.pos.CashRegister;
import eapli.ecafeteria.domain.cafeteria.OrganicUnit;
import eapli.ecafeteria.persistence.CashRegisterRepository;
import eapli.ecafeteria.persistence.OrganicUnitRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;

/**
 *
 * @author Jorge Esteves <1150742@isep.ipp.pt>
 */
public class CashRegisterController implements Controller {
    
    private final CashRegisterRepository cashRegisterRepository = PersistenceContext.repositories().cashRegisters();
    
    private final OrganicUnitRepository orgaUnitRepository = PersistenceContext.repositories().organicUnits();
    
    public CashRegister registerCashRegister(String acronym, String name, String description, OrganicUnit orga)
            throws DataIntegrityViolationException, DataConcurrencyException {
        Application.ensurePermissionOfLoggedInUser(ActionRight.ADMINISTER);
        
        return this.cashRegisterRepository.save(new CashRegister(acronym, name, description, orga));
    }
    
    public Iterable<OrganicUnit> organicUnits(){
        return this.orgaUnitRepository.findAll();
    }
}
