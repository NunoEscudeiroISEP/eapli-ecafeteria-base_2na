/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.pos;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.pos.Complaint;
import eapli.ecafeteria.persistence.ComplaintRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;

/**
 *
 * @author Topa
 */
public class RegisterComplaintController implements Controller {
    private final ComplaintRepository repository = PersistenceContext.repositories().complaints();

    public Complaint registerComplaint(final CafeteriaUser user, Meal meal, String description) throws DataIntegrityViolationException, DataConcurrencyException {

        Application.ensurePermissionOfLoggedInUser(ActionRight.ADMINISTER);

        final Complaint newComplaint = new Complaint(user, meal, description);

        Complaint ret = this.repository.save(newComplaint);

        return ret;
    }
    
}
