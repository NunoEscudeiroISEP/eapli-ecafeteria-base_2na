/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.pos;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.meals.Booking;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.BookingRepository;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;


/**
 *
 * @author Topa
 */
public class BookingMealDeliveryController implements Controller {

    private final BookingRepository bookingRepository = PersistenceContext.repositories().bookings();
    private final MealRepository mealRepository = PersistenceContext.repositories().meals();
    
    public Iterable<Booking> getBookingsByUser(CafeteriaUser user) {
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_DELIVERY);
        if (user == null) {
            throw new IllegalArgumentException();
        }
        // verificar se esta ppublicado num menu
        // Perguntar ao menu quais as meals que existem para este dia
        return bookingRepository.getAllBookingsFromUser(user);
    }

    public boolean ChangeBookingStatusToDelivered(Booking booking) throws DataConcurrencyException, DataIntegrityViolationException {
        if (booking == null) {
            throw new IllegalArgumentException();
        }
        // check permissions
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_DELIVERY);

        if (booking.status().changeToDelivered()) {
            Meal meal=booking.getMeal();
            int vendidos=meal.Quantidade().mealsSolded()+1;
            booking.getMeal().Quantidade().updateSoldedMeals(vendidos);
            mealRepository.save(meal);
            bookingRepository.save(booking);
            
            return true;
        } else {
            return false;

        }
    }

    public boolean ChangeBookingStatusToCAncelled(Booking booking) throws DataConcurrencyException, DataIntegrityViolationException {
        if (booking == null) {
            throw new IllegalArgumentException();
        }
        // check permissions
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_DELIVERY);

        if (booking.status().changeToCancelled()) {

            bookingRepository.save(booking);
            return true;
        } else {
            return false;

        }
    }
}
