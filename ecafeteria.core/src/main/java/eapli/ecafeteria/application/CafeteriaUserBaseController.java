/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.domain.Money;

/**
 *
 * @author mcn
 */
public class CafeteriaUserBaseController implements Controller {

    private final CafeteriaUserRepository customerRepository = PersistenceContext.repositories().cafeteriaUsers(true);
    
    public Money balance() {
        SystemUser SystemUser =  Application.session().session().authenticatedUser();
        CafeteriaUser user = customerRepository.findByUsername(SystemUser.username());
        return user.balance().money();
    }  
}
