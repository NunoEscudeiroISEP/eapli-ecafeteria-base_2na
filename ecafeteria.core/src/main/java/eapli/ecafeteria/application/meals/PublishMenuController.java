/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.meals;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.ecafeteria.persistence.MenuRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;

/**
 *
 * @author Daniel Correia
 */
public class PublishMenuController implements Controller{
    
    private final ListMenuService svcMN = new ListMenuService();
    private final MenuRepository menuRepository = PersistenceContext.repositories().menus();

    public Iterable<Menu> allInProgressMenus() {
        return this.svcMN.allInProgressMenus();
    }

    public void changeMenuStateToPublished(Menu menu) throws DataConcurrencyException, DataIntegrityViolationException {
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_MENUS);
        if (menu == null) {
            throw new IllegalArgumentException();
        }
        menu.changeStateToPublished();

        Menu ret = this.menuRepository.save(menu);
    }
}
