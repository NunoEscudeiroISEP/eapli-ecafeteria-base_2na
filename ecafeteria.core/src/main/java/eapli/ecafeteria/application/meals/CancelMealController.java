/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.meals;

import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.date;
import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.meals.Booking;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.exceptions.MealBookingException;
import eapli.ecafeteria.domain.pos.BookingStatus;
import eapli.ecafeteria.persistence.BookingRepository;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;

/**
 *
 * @author Cláudia
 */
public class CancelMealController implements Controller{
    private final BookingRepository bookingRepository = PersistenceContext.repositories().bookings();
    private final CafeteriaUserRepository customerRepository = PersistenceContext.repositories().cafeteriaUsers(true);
    
     public Iterable<Booking> getAllBookings(CafeteriaUser user){
       Application.ensurePermissionOfLoggedInUser(ActionRight.SELECT_MEAL);
       if (user == null) {
           throw new IllegalArgumentException();
       }
     
       return bookingRepository.getAllBookingsFromUser(user);
    }
     
    public Booking cancelBooking(Booking book)throws DataConcurrencyException, DataIntegrityViolationException{
       if (book==null)
           throw new IllegalArgumentException();
       
       Application.ensurePermissionOfLoggedInUser(ActionRight.SELECT_MEAL);
       
       SystemUser systemUser = Application.session().session().authenticatedUser();
       CafeteriaUser customer = customerRepository.findByUsername(systemUser.username());
               
       String booking = book.toString();
       
       if(!booking.isEmpty())
          book.status().changeToCancelled();

       return bookingRepository.save(book);
    }
}
