package eapli.ecafeteria.application.meals;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.Allergen;
import eapli.ecafeteria.persistence.AllergenRepository;
import eapli.ecafeteria.persistence.PersistenceContext;

/**
 * @author 1141045@isep.ipp.pt
 * @author 1090956@isep.ipp.pt
 */
public class ListAllergenService {

    private AllergenRepository allergenRepository = PersistenceContext.repositories().allergens();

    public Iterable<Allergen> allAllergens() {
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_MENUS);

        return this.allergenRepository.findAll();
    }
}
