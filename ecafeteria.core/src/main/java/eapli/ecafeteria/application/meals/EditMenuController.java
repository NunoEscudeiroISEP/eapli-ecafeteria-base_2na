/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.meals;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.kitchen.Quantity;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.MenuRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Daniel Correia
 */
public class EditMenuController implements Controller {

    private final ListDishService svcDS = new ListDishService();
    private final ListMenuService svcMN = new ListMenuService();
    private final ListMealTypeService svcMT = new ListMealTypeService();
    private final MenuRepository menuRepository = PersistenceContext.repositories().menus();
    private final MealRepository mealRepository = PersistenceContext.repositories().meals();
    private Menu selectedMenu, menu_clone;

    public Iterable<Menu> allInProgressMenus() {
        return this.svcMN.allInProgressMenus();
    }

    public Iterable<Meal> allMealsfromMenu() {
        return menu_clone.meals();
    }

    public Iterable<Dish> allDishes() {
        return this.svcDS.allDishes();
    }
    
    public Iterable<MealType> mealTypes() {
        return this.svcMT.allMealTypes();
    }

    public void addMealToMenu(Date mealDate, Dish dish, MealType mealType, Quantity qtd) throws DataConcurrencyException, DataIntegrityViolationException {
        Meal meal = new Meal(mealDate, dish, mealType, qtd);
        if (menu_clone.addMeal(meal)) {
            mealRepository.save(meal);
        }
    }

    public void makeMenuStateToPublished() {
        menu_clone.changeStateToPublished();
    }

    public Menu getMenuToPrint() {
        return menu_clone;
    }

    public void selectMenu(Menu selectedMenu) throws CloneNotSupportedException {
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_MENUS);
        this.selectedMenu = selectedMenu;
        this.menu_clone = this.selectedMenu.clone();
    }

    public void setInitialDate(Date initialDate) {
        for (Meal meal : this.selectedMenu.meals()) {
            if (meal.date().compareTo(initialDate) == -1) {
                menu_clone.meals().remove(meal);
            }
        }
        menu_clone.setInitialDate(initialDate);
    }

    public void setFinalDate(Date finalDate) {
        for (Meal meal : this.selectedMenu.meals()) {
            if (meal.date().compareTo(finalDate) == 1) {
                removeMeal(meal);
            }
        }
        menu_clone.setFinalDate(finalDate);
    }

    public void removeMeal(Meal meal) {
        menu_clone.removeMeal(meal);
    }

    public void addMeal(Meal meal) {
        menu_clone.addMeal(meal);
    }

    public long MenuPeriod() {
        return menu_clone.period();
    }

    public Date MenuInitialDate() {
        return menu_clone.initialDate();
    }

    public void editMenu() throws DataConcurrencyException, DataIntegrityViolationException {
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_MENUS);
        this.menuRepository.delete(selectedMenu);
        Menu ret = this.menuRepository.save(menu_clone);
    }
}
