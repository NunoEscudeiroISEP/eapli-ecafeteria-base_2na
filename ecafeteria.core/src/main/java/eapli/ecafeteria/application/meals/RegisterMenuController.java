/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.meals;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.kitchen.Quantity;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.MenuRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Daniel Correia
 */
public class RegisterMenuController implements Controller {

    private final MenuRepository menuRepository = PersistenceContext.repositories().menus();
    private final MealRepository mealRepository = PersistenceContext.repositories().meals();
    private final ListDishService svcDS = new ListDishService();
    private final ListMealTypeService svcMT = new ListMealTypeService();
    private Menu menu;

    public Iterable<Dish> allDishes() {
        return this.svcDS.allDishes();
    }

    public Iterable<MealType> mealTypes() {
        return this.svcMT.allMealTypes();
    }

    public boolean startRegisterMenu(Date initialDate, Date finalDate) {
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_MENUS);
        this.menu = new Menu(initialDate, finalDate);
        if (menu.validateDates(initialDate, finalDate)) {
            menu.changeStateToInProgress();
            return true;
        }
        return false;
    }

    public long MenuPeriod() {
        return menu.period();
    }

    public Menu getMenuToPrint() {
        return menu;
    }

    public void addMealToMenu(Date mealDate, Dish dish, MealType mealType, Quantity qtd) throws DataConcurrencyException, DataIntegrityViolationException {
        Meal meal = new Meal(mealDate, dish, mealType, qtd);
        if (menu.addMeal(meal)) {
            mealRepository.save(meal);
        }
    }

    public void makeMenuStateToPublished() {
        menu.changeStateToPublished();
    }

    public Menu registerMenu() throws DataConcurrencyException, DataIntegrityViolationException {
        return menuRepository.save(menu);
    }

    public Menu registerMenu(final Date initialdate, final Date finalDate, final List<Meal> mealList, final boolean stateinProgress, final boolean statePublish) throws DataConcurrencyException, DataIntegrityViolationException {
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_MENUS);

        final Menu newMenu = new Menu(initialdate, finalDate, mealList, stateinProgress, statePublish);

        Menu ret = this.menuRepository.save(newMenu);

        return ret;
    }

}
