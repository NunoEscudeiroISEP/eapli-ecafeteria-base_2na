/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.meals;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.meals.Booking;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.ecafeteria.domain.meals.exceptions.MealBookingException;
import eapli.ecafeteria.persistence.BookingRepository;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.MenuRepository;
import eapli.framework.application.Controller;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.domain.Money;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.Date;
import javax.persistence.Persistence;
/**
 *
 * @author marcio
 */
public class BookingMealController implements Controller{
    
    private final MealRepository mealRepository = PersistenceContext.repositories().meals();
    private final CafeteriaUserRepository customerRepository = PersistenceContext.repositories().cafeteriaUsers(true);
    private final BookingRepository bookingRepository = PersistenceContext.repositories().bookings();
    private final MenuRepository menuRepository = PersistenceContext.repositories().menus();
    
    public Iterable<Meal> getMealsByDate(Date date){
       Application.ensurePermissionOfLoggedInUser(ActionRight.SELECT_MEAL);
       if (date == null) {
           throw new IllegalArgumentException();
       }
       // verificar se esta ppublicado num menu
       // Perguntar ao menu quais as meals que existem para este dia
       //return menuRepository.findByDay(date).meals();
       return mealRepository.findByDate(date);
    }
    
    public Booking bookingMeal(Meal meal) throws DataConcurrencyException, DataIntegrityViolationException{
        if(meal == null)
            throw new IllegalArgumentException();
        // check permissions
        Application.ensurePermissionOfLoggedInUser(ActionRight.SELECT_MEAL);
        // get CafetariaUser by session
        SystemUser systemUser = Application.session().session().authenticatedUser();
        CafeteriaUser customer = customerRepository.findByUsername(systemUser.username());
        // check if customer have money to pay
        Money mealPrice = meal.dish().currentPrice();
        if(!customer.balance().canPay(mealPrice))
            throw new MealBookingException("the customer "+customer.mecanographicNumber().toString()+" doesn't have enough money to book this meal");
        // create booking
        Booking booking = new Booking(meal, customer);
        // update balance
        if(!customer.balance().Pay(mealPrice))
             throw new MealBookingException("An error occurred while trying to pay for the meal");
        // save and return booking
        customerRepository.save(customer);
        return bookingRepository.save(booking);
    }
    
    
}
