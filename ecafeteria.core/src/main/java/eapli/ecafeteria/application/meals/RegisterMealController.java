/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.meals;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.kitchen.Quantity;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.Date;

/**
 *
 * @author Daniel Correia
 */
public class RegisterMealController implements Controller {
    
    private final ListMealTypeService svcMT= new ListMealTypeService();
    
    private final MealRepository mealRepository = PersistenceContext.repositories().meals();
    
    public Meal registerMeal(final Date date, final Dish dish, final MealType mealType, Quantity qtd) throws DataConcurrencyException, DataIntegrityViolationException{
         Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_MENUS);

        final Meal newMeal = new Meal(date, dish, mealType,qtd);


        Meal ret = this.mealRepository.save(newMeal);

        return ret;
    }
    
    public Iterable<MealType> mealTypes() {
        return this.svcMT.allMealTypes();
    }
}
