package eapli.ecafeteria.application.meals;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.Allergen;
import eapli.ecafeteria.persistence.AllergenRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;

import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;


/**
 * @author 1141045@isep.ipp.pt
 * @author 1090956@isep.ipp.pt
 */
public class RegisterAllergenController implements Controller {

    private AllergenRepository allergenRepository = PersistenceContext.repositories().allergens();

    public Allergen registerDish(final String name) throws DataConcurrencyException, DataIntegrityViolationException {

        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_MENUS);


        final Allergen newAllergen = new Allergen(name);

        Allergen ret = this.allergenRepository.save(newAllergen);

        return ret;
    }
}
