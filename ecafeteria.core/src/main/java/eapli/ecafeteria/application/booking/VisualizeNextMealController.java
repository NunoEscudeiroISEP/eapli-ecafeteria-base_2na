package eapli.ecafeteria.application.booking;

import eapli.ecafeteria.persistence.BookingRepository;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.authz.Username;
import eapli.ecafeteria.domain.meals.Booking;
import java.util.Iterator;


/**
 * Created by dinis on 18/05/2017.
 * Controller for use case RR03.
 */
public class VisualizeNextMealController {// implements Controller {
//
//  private static String ERR_MSG_NULL_USER;
//  private static String ERR_MSG_INVALID_PERMISSIONS;
//  private static String ERR_MSG_NO_BOOKINGS_FOUND;
//  private static String ERR_MSG_UNABLE_ACCESS_DATABASE;
//  private static String ERR_MSG_NULL_USERNAME;
//  private static String ERR_MSG_NO_CAF_USER_ASSIGNED;
//
//  /**
//   * The repository that allows this controller to search for user bookings.
//   */
//  private final BookingRepository bookingRepository = PersistenceContext.repositories().bookings();
//  private final CafeteriaUserRepository userRepository = PersistenceContext.repositories().cafeteriaUsers(true);
//
//  // TODO move this
//  public static class VisualizeMealException extends RuntimeException{public VisualizeMealException(String message) {super(message);}}
//  
//  // TODO move this
//  public static class BookedMealViewModel {
//
//    String mealDate;
//    String mealType;
//    String dish;
//    String dishType;
//    // TODO do this
//    private static BookedMealViewModel build(Booking earliestBooking) {
//      return new BookedMealViewModel();
//    }
// }
//  
//  public BookedMealViewModel visualizeNextMeal(){
//    
//    // Set output value
//    BookedMealViewModel output = null;
//    
//    // Get session user
//    SystemUser user = Application.session().session().authenticatedUser();
//    
//    // Validate and retrieve cafeteria user
//    CafeteriaUser cafeteriaUser = this.getCafeteriaUser(user);
//     
//    // Search for next booking
//    Booking earliestBooking = this.getLatestBooking(cafeteriaUser);
//     
//    // Build and retrun a BookedMealViewModel
//    return BookedMealViewModel.build(earliestBooking);
//  } 
//
//  /**
//   * Verifies if the user has the necessary permission to acces this functinality.
//   * User must be able to select meals and be of role cafeteria user.
//   * @param user
//   * @return 
//   */
//  private boolean doesUserHavePermissions(SystemUser user) {
//    boolean output = false;
//    if (user != null) {
//      output = user.isAuthorizedTo(ActionRight.SELECT_MEAL);
//    }
//    return output;
//  }
//
//  /**
//   * TODO test
//   * @param user
//   * @return 
//   */
//  private Booking getLatestBooking(CafeteriaUser user) {
//    Booking output = null;  
//    if (user != null) {
//      Iterable<Booking> allUserBookings = this.bookingRepository.getAllBookingsFromUser(user);
//      if (allUserBookings == null || allUserBookings.iterator() == null || !allUserBookings.iterator().hasNext()) {
//        throw new VisualizeMealException(VisualizeNextMealController.ERR_MSG_NO_BOOKINGS_FOUND);
//      }
//      
//      Iterator<Booking> iterator = allUserBookings.iterator();
//      output = iterator.next();
//      while (iterator.hasNext()) {
//        Booking next = iterator.next();
//        if (next.getMeal().date().before(output.getMeal().date())) {
//          output = next;
//        }
//      }
//    }
//    return output;
//  }
//  
//  /**
//   * Validates the user.
//   * TODO test
//   * @param user
//   * @return <code>true</code> if the user is valid, <code>true</code> otherwisse.
//   */
//  public CafeteriaUser getCafeteriaUser(SystemUser user){
//    
//    if (user == null) {
//      throw new VisualizeMealException(VisualizeNextMealController.ERR_MSG_NULL_USER);
//    }
//    
//    Username username = user.username();
//    
//    if (username == null) {
//      throw new VisualizeMealException(VisualizeNextMealController.ERR_MSG_NULL_USERNAME);
//    }
//    
//    if (!this.doesUserHavePermissions(user)) {
//      throw new VisualizeMealException(VisualizeNextMealController.ERR_MSG_INVALID_PERMISSIONS);
//    }
//    
//    if (this.userRepository == null) {
//      throw new VisualizeMealException(VisualizeNextMealController.ERR_MSG_UNABLE_ACCESS_DATABASE);
//    }
//    
//    CafeteriaUser cafeteriaUser = this.userRepository.findByUsername(username);
//    
//    if (cafeteriaUser == null) {
//      throw new VisualizeMealException(VisualizeNextMealController.ERR_MSG_NO_CAF_USER_ASSIGNED);
//    }
//    
//    return cafeteriaUser;
//  }
//  
//  /*public DishType registerDishType(String acronym, String description)
//      throws DataIntegrityViolationException, DataConcurrencyException {
//    Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_MENUS);
//
//    final DishType newDishType = new DishType(acronym, description);
//    return this.repository.save(newDishType);
//  }*/
}
