package eapli.ecafeteria.application.kitchen;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.kitchen.KitchenAlert;
import eapli.ecafeteria.persistence.KitchenAlertRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;

/**
 * Created by vascopinho on 04/05/17.
 */
public class SetKitchenAlertLimitController implements Controller {

    private final KitchenAlertRepository repository = PersistenceContext.repositories().kitchenAlert();

    public KitchenAlert setKitchenLimits(int yellow, int red) throws DataIntegrityViolationException,DataConcurrencyException {
        Application.ensurePermissionOfLoggedInUser(ActionRight.ADMINISTER);
        final KitchenAlert kitchenAlert = new KitchenAlert(yellow,red);
        return this.repository.save(kitchenAlert);
    }


    public KitchenAlert getKitchenLimits() throws DataIntegrityViolationException,DataConcurrencyException{
        Application.ensurePermissionOfLoggedInUser(ActionRight.ADMINISTER);
        return this.repository.getKitchenAlert();
    }
}
