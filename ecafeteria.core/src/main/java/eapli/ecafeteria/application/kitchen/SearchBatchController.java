/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.kitchen;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.kitchen.Batch;
import eapli.ecafeteria.persistence.BatchRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Daniel
 */
public class SearchBatchController implements Controller{
    
    private final BatchRepository repository = PersistenceContext.repositories().batches();
    
    public Iterable<Batch> searchBatchByNumber(String batchNumber)
            throws DataIntegrityViolationException, DataConcurrencyException {
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_KITCHEN);

        final Batch batch = new Batch(batchNumber);
        
        return this.repository.findByBatchNumber(batch.getBatchnumber());
    }
    
    public Iterable<Batch> searchBatchByUsedDate(Date usedDate)
            throws DataIntegrityViolationException, DataConcurrencyException {
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_KITCHEN);

        final Batch batch = new Batch(usedDate);
        
        return this.repository.findByUsedDate(batch.getDayUsed());
    }
    
}
