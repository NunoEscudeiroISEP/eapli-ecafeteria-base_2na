package eapli.ecafeteria.application.cafeteria;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.cafeteria.OrganicUnit;
import eapli.ecafeteria.persistence.OrganicUnitRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.util.DateTime;

/**
 * Created by vascopinho on 23/05/17.
 */
public class DeactivateOragnicUnitController implements Controller {

    private final OrganicUnitRepository organicUnitRepository = PersistenceContext.repositories().organicUnits();

    public Iterable<OrganicUnit> activeOrganicUnits() {
        Application.ensurePermissionOfLoggedInUser(ActionRight.ADMINISTER);
        return this.organicUnitRepository.findActive();
    }

    public OrganicUnit deactivateOrganicUnit(OrganicUnit organicUnit) throws DataConcurrencyException, DataIntegrityViolationException {
        Application.ensurePermissionOfLoggedInUser(ActionRight.ADMINISTER);

        organicUnit.deactivate(DateTime.now());
        return this.organicUnitRepository.save(organicUnit);
    }

}
