package eapli.ecafeteria.application.authz;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.UserRepository;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;

/**
 * Created by vascopinho on 24/05/17.
 */
public class ResetPasswordController implements Controller {

    private final UserRepository userRepository = PersistenceContext.repositories().users(true);

    public Iterable<SystemUser> getAllUsers(){
        Application.ensurePermissionOfLoggedInUser(ActionRight.ADMINISTER);
        return this.userRepository.findAll();
    }


    public void resetPassword(SystemUser systemUser) throws DataConcurrencyException, DataIntegrityViolationException {
        try {
            systemUser.resetPassword();
        } catch (IllegalArgumentException e){
            System.out.println(e.getMessage());
        }

        this.userRepository.save(systemUser);

    }
}
