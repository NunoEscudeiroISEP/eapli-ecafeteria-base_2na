package eapli.ecafeteria.application.authz;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.authz.UserAlert;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.UserAlertRepository;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;

/**
 * Created by vascopinho on 18/05/17.
 */
public class SetUserAlertLimitController implements Controller {
    private final UserAlertRepository repository = PersistenceContext.repositories().userAlert();

    public UserAlert setUserLimits(int limit) throws DataIntegrityViolationException,DataConcurrencyException {
        Application.ensurePermissionOfLoggedInUser(ActionRight.ADMINISTER);
        final UserAlert userAlert = new UserAlert(limit);
        return this.repository.save(userAlert);
    }


    public UserAlert getUserLimits() throws DataIntegrityViolationException,DataConcurrencyException{
        Application.ensurePermissionOfLoggedInUser(ActionRight.ADMINISTER);
        return this.repository.getUserAlert();
    }
}
