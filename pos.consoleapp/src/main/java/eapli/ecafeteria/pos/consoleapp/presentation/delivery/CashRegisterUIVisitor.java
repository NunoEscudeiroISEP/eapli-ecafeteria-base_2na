/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.pos.consoleapp.presentation.delivery;

import eapli.ecafeteria.domain.pos.CashRegister;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author Carlos Pinho & Jorge Esteves
 */
public class CashRegisterUIVisitor implements Visitor<CashRegister> {

    @Override
    public void visit(CashRegister visited) {
        System.out.println(visited.id() + " " + visited.name() + " " + visited.organic() + " " +  visited.isOpen());
    }
    
    @Override
    public void beforeVisiting(CashRegister visited) {
        System.out.println("Acronimo Name Organic Unit Open");
    }
}
