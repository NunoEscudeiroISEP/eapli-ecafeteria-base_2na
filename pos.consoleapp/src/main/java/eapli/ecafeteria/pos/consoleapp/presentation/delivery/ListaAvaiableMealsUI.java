/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.pos.consoleapp.presentation.delivery;

import eapli.ecafeteria.application.pos.AvailableMealsController;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;

/**
 *
 * @author Topa
 */
public class ListaAvaiableMealsUI extends AbstractUI {

    private final AvailableMealsController theController = new AvailableMealsController();
    Date date = new Date();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
       HashMap<Meal, Integer> map = theController.getAvailableMealsToBook(date);
       showMeals(map);
        return true;
    }

    

    @Override
    public String headline() {
        return ("Avaiable Meals");
    }

    private void showMeals(HashMap<Meal, Integer> map) {
      
        System.out.println(Arrays.asList(map));
    }

}
