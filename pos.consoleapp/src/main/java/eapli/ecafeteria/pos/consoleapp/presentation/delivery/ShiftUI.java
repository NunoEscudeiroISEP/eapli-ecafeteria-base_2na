/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.pos.consoleapp.presentation.delivery;

import eapli.ecafeteria.domain.pos.Shift;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author CarlosPinho (1980380)
 */
public class ShiftUI implements Visitor<Shift> {
    
     @Override
    public void visit(Shift visited) {
        System.out.println(visited.id() + " " + visited.openDate().toString());
    }
    
    @Override
    public void beforeVisiting(Shift visited) {
        System.out.println("Acronimo Name Organic Unit Open");
    }
    
}
