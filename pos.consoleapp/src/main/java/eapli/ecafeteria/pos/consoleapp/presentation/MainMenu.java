/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */
package eapli.ecafeteria.pos.consoleapp.presentation;

import eapli.cafeteria.consoleapp.presentation.ExitWithMessageAction;
import eapli.cafeteria.consoleapp.presentation.MyUserMenu;
import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.pos.consoleapp.presentation.delivery.BookingMealCancelledAction;
import eapli.ecafeteria.pos.consoleapp.presentation.delivery.BookingMealDeliveryAction;
import eapli.ecafeteria.pos.consoleapp.presentation.delivery.ListaAvaiableMealsAction;
import eapli.ecafeteria.pos.consoleapp.presentation.delivery.OpenShiftAction;
import eapli.ecafeteria.pos.consoleapp.presentation.delivery.RegisterCommentAction;
import eapli.ecafeteria.pos.consoleapp.presentation.delivery.RegisterComplaintAction;
import eapli.framework.actions.ReturnAction;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.HorizontalMenuRenderer;
import eapli.framework.presentation.console.Menu;
import eapli.framework.presentation.console.MenuItem;
import eapli.framework.presentation.console.MenuRenderer;
import eapli.framework.presentation.console.ShowVerticalSubMenuAction;
import eapli.framework.presentation.console.SubMenu;
import eapli.framework.presentation.console.VerticalMenuRenderer;
import eapli.framework.presentation.console.VerticalSeparator;

/**
 * TODO split this class in more specialized classes for each menu
 *
 * @author Paulo Gandra Sousa
 */
public class MainMenu extends AbstractUI {

    private static final int EXIT_OPTION = 0;

    // MAIN MENU
    private static final int MY_USER_OPTION = 1;
    private static final int OPEN_SHIFT = 2;
    private static final int NEW_COMPLAINT = 3;
    private static final int NEW_COMMENT = 4;

    //Open shift options
    private static final int OPEN_CASH_REGISTER_OPTION = 1;
    private static final int CHANGE_BOOKINGSTATUS_TO_DELIVER = 2;
    private static final int CHANGE_BOOKINGSTATUS_TO_CANCELLED = 3;
    private static final int LIST_AVAIABLE_MEAL = 4;
    //Menu Booking Meal Deliver

    //comlaint's and comments
    @Override
    public boolean show() {
        drawFormTitle();
        return doShow();
    }

    /**
     * @return true if the user selected the exit option
     */
    @Override
    public boolean doShow() {
        final Menu menu = buildMainMenu();
        final MenuRenderer renderer;
        if (Application.settings().isMenuLayoutHorizontal()) {
            renderer = new HorizontalMenuRenderer(menu);
        } else {
            renderer = new VerticalMenuRenderer(menu);
        }
        return renderer.show();
    }

    @Override
    public String headline() {
        return "eCafeteria POS [@" + Application.session().session().authenticatedUser().id() + "]";
    }

    private Menu buildMainMenu() {
        final Menu mainMenu = new Menu();

        final Menu myUserMenu = new MyUserMenu();
        mainMenu.add(new SubMenu(MY_USER_OPTION, myUserMenu, new ShowVerticalSubMenuAction(myUserMenu)));

        if (!Application.settings().isMenuLayoutHorizontal()) {
            mainMenu.add(VerticalSeparator.separator());
        }

        if (Application.session().session().authenticatedUser().isAuthorizedTo(ActionRight.SALE)) {

            final Menu shiftMenu = buildShiftMenu();

            mainMenu.add(new SubMenu(OPEN_SHIFT, shiftMenu, new ShowVerticalSubMenuAction(shiftMenu)));

        }

        if (Application.session().session().authenticatedUser().isAuthorizedTo(ActionRight.SALE)) {

            final Menu shiftMenu = buildCommentMenu();

            mainMenu.add(new SubMenu(NEW_COMMENT, shiftMenu, new ShowVerticalSubMenuAction(shiftMenu)));

        }

        if (Application.session().session().authenticatedUser().isAuthorizedTo(ActionRight.SALE)) {

            final Menu shiftMenu = buildComplaintMenu();

            mainMenu.add(new SubMenu(NEW_COMPLAINT, shiftMenu, new ShowVerticalSubMenuAction(shiftMenu)));

        }

        if (!Application.settings().isMenuLayoutHorizontal()) {
            mainMenu.add(VerticalSeparator.separator());
        }

        mainMenu.add(new MenuItem(EXIT_OPTION, "Exit", new ExitWithMessageAction()));

        return mainMenu;
    }

    private Menu buildShiftMenu() {
        final Menu menu = new Menu("Cash Register Selector >");

        menu.add(new MenuItem(OPEN_CASH_REGISTER_OPTION, "Select Cash Register",
                new OpenShiftAction()));

        menu.add(new MenuItem(CHANGE_BOOKINGSTATUS_TO_DELIVER, "Change the Booking to Deliver",
                new BookingMealDeliveryAction()));

        menu.add(new MenuItem(CHANGE_BOOKINGSTATUS_TO_CANCELLED, "Change the Booking to Cancelled",
                new BookingMealCancelledAction()));

        menu.add(new MenuItem(LIST_AVAIABLE_MEAL, "Still Avaiable Meals by type ",
                new ListaAvaiableMealsAction()));

        menu.add(new MenuItem(EXIT_OPTION, "Return ", new ReturnAction()));

        return menu;

    }

    private Menu buildComplaintMenu() {
        final Menu menu = new Menu("Complaint Register >");

        menu.add(new MenuItem(NEW_COMPLAINT, "Register a Complaint", new RegisterComplaintAction()));

        menu.add(new MenuItem(EXIT_OPTION, "Return ", new ReturnAction()));

        return menu;
    }

    private Menu buildCommentMenu() {
        final Menu menu = new Menu("Comment's Register >");

        menu.add(new MenuItem(NEW_COMMENT, "Register a Commments", new RegisterCommentAction()));

        menu.add(new MenuItem(EXIT_OPTION, "Return ", new ReturnAction()));

        return menu;
    }
}
