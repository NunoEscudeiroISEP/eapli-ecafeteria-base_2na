/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.pos.consoleapp.presentation.delivery;


import eapli.framework.actions.Action;

/**
 *
 * @author Topa
 */
public class BookingMealDeliveryAction implements Action{
     @Override
    public boolean execute() {
        return new BookingMealDeliveryUI().doShow();
    }
    
}
