/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.pos.consoleapp.presentation.delivery;

import eapli.ecafeteria.application.CafeteriaUserBaseController;
import eapli.ecafeteria.application.meals.ListMealController;
import eapli.ecafeteria.application.meals.ListMealService;
import eapli.ecafeteria.application.meals.RegisterMealController;
import eapli.ecafeteria.application.pos.RegisterComplaintController;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;

/**
 *
 * @author Helder
 */
public class RegisterComplaintUI extends AbstractUI {
    
    
    private final RegisterComplaintController theController = new RegisterComplaintController();
    
    protected Controller controller() {
        return (Controller) this.theController;
    }

    @Override
    protected boolean doShow() {
         // para alterar
//        Material material = null;
        
        
        return false;
    }

    @Override
    public String headline() {
          return "Register a Complaint";
    }
    
}
