/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.pos.consoleapp.presentation.delivery;

import eapli.ecafeteria.application.pos.OpenShiftController;
import eapli.ecafeteria.domain.pos.CashRegister;
import eapli.ecafeteria.domain.pos.Shift;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.Console;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * **
 */
/**
 *
 * @author Carlos Pinho & Jorge Esteves
 */
public class OpenShiftUI extends AbstractUI {

    private final OpenShiftController theController = new OpenShiftController();

    protected Controller controller() {
        return (Controller) this.theController;
    }

    @Override
    protected boolean doShow() {
        Date openTime = null;
        final String data;
        final DateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        System.out.println("\n \t Do you want to Set the current date and time as working timesamp ? (Y/N)");
        String answer = Console.readLine("Answer");
        if (answer.equalsIgnoreCase("Y")) {

            data = sdf.format(new Date());
            try {
                openTime = sdf.parse(data);
            } catch (ParseException e) {
                System.out.println(e);
            }
        } else {
            System.out.println("Whrite a new Date and Time (dd-MM-yyyy HH:mm:ss)");
            data = Console.readLine("Date and Time");
            try {
                openTime = sdf.parse(data);
            } catch (ParseException e) {
                System.out.println(e);
            }
        }

        final SelectWidget<CashRegister> selector_cashRegister = new SelectWidget<>(this.theController.cashRegisters(),
                new CashRegisterUIVisitor());
        selector_cashRegister.show();

        final CashRegister cashRegister = selector_cashRegister.selectedElement();

        boolean openCashRegisterOk = false;

        try {
            openCashRegisterOk = this.theController.openCashRegister(openTime, cashRegister);
        } catch (final DataIntegrityViolationException | DataConcurrencyException e) {
            //error from database connection
        }

        if (!openCashRegisterOk) {
            System.out.println("Cash Register: "+cashRegister.name()+" Selected!");
            //return false;             
        } else {
            System.out.println("Cash Register: "+cashRegister.name()+" Selected!");
        }

        try {
            this.theController.openShift(openTime, cashRegister);
        } catch (final DataIntegrityViolationException | DataConcurrencyException e) {
            //error from database connection
        }

        final SelectWidget<Shift> selector_shifts = new SelectWidget<>(this.theController.shiftS(),
                new ShiftUI());
        selector_shifts.show();

        final Shift shift = selector_shifts.selectedElement();

        return false;

    }

    @Override
    public String headline() {
        return "Open Cash Register Shift";
    }

}
