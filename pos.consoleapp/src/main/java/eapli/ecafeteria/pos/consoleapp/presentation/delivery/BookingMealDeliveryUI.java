/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.pos.consoleapp.presentation.delivery;

import eapli.ecafeteria.application.cafeteria.CafeteriaUserService;
import eapli.ecafeteria.application.pos.BookingMealDeliveryController;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.meals.Booking;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;
import java.io.IOException;
import static java.lang.System.in;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Topa
 */
public class BookingMealDeliveryUI extends AbstractUI {

    private final BookingMealDeliveryController theControllerMealDelivery = new BookingMealDeliveryController();
    private final CafeteriaUserService theServiceCafeteriaUser = new CafeteriaUserService();

    protected Controller controller() {
        return this.theControllerMealDelivery;
    }

    @Override
    protected boolean doShow() {

        Date initialDate = defineDate("initial");
        Date finalDate = defineDate("final");
        Date mealDate = new Date(initialDate.getYear(), initialDate.getMonth(), initialDate.getDate() - 1);
        //read client to search the booking
        CafeteriaUser userClient = defineClientUser();
        //select booking to change statusclient

        Iterable<Booking> books = theControllerMealDelivery.getBookingsByUser(userClient);

        List<Booking> list = new ArrayList<Booking>();
        int controloador = 0;
        System.out.println("Choose the desire booking \n");
        for (Booking item : books) {
            System.out.println(controloador + " : " + item.toString());
            list.add(item);
        }

        int choosed = 0;
        try {
            choosed = in.read();
        } catch (IOException ex) {
            Logger.getLogger(BookingMealDeliveryUI.class.getName()).log(Level.SEVERE, null, ex);
        }

        Booking book = list.get(choosed);

        try {
            //ultima coisa
            theControllerMealDelivery.ChangeBookingStatusToDelivered(book);
        } catch (DataConcurrencyException ex) {
            Logger.getLogger(BookingMealDeliveryUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DataIntegrityViolationException ex) {
            Logger.getLogger(BookingMealDeliveryUI.class.getName()).log(Level.SEVERE, null, ex);
        }

        return true;
    }

    public Date defineDate(String typeDate) {
        System.out.println("Enter the " + typeDate + " date for the menu:");
        return Console.readDate("YYYY/MM/DD:");
    }

    @Override
    public String headline() {
        return "Create a new Menu";
    }

    private CafeteriaUser defineClientUser() {
        System.out.println("Enter the Client Mecanographic Number");
        String in = (new Scanner(System.in)).nextLine();
        CafeteriaUser user = theServiceCafeteriaUser.findCafeteriaUserByMecNumber(in);
        return user;

    } 
    protected boolean doShow2() {

        Date initialDate = defineDate("initial");
        Date finalDate = defineDate("final");
        Date mealDate = new Date(initialDate.getYear(), initialDate.getMonth(), initialDate.getDate() - 1);
        //read client to search the booking
        CafeteriaUser userClient = defineClientUser();
        //select booking to change statusclient

        Iterable<Booking> books = theControllerMealDelivery.getBookingsByUser(userClient);

        List<Booking> list = new ArrayList<Booking>();
        int controloador = 0;
        System.out.println("Choose the desire booking \n");
        for (Booking item : books) {
            System.out.println(controloador + " : " + item.toString());
            list.add(item);
        }

        int choosed = 0;
        try {
            choosed = in.read();
        } catch (IOException ex) {
            Logger.getLogger(BookingMealDeliveryUI.class.getName()).log(Level.SEVERE, null, ex);
        }

        Booking book = list.get(choosed);

        try {
            //ultima coisa
            theControllerMealDelivery.ChangeBookingStatusToCAncelled(book);
        } catch (DataConcurrencyException ex) {
            Logger.getLogger(BookingMealDeliveryUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DataIntegrityViolationException ex) {
            Logger.getLogger(BookingMealDeliveryUI.class.getName()).log(Level.SEVERE, null, ex);
        }

        return true;
    }
  
    
    
    

}
