/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.user.consoleapp.presentation.authz;

import eapli.framework.application.Controller;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import eapli.ecafeteria.application.cafeteria.SignupController;
import eapli.ecafeteria.domain.cafeteria.OrganicUnit;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.Console;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Cláudia
 */
public class SignupRequestUIIT {
    
    public SignupRequestUIIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of controller method, of class SignupRequestUI.
     */
    @Test
    public void testController() {
        System.out.println("controller");
        SignupRequestUI instance = new SignupRequestUI();
        SignupController expResult = new SignupController();
        Controller result = instance.controller();
        assertEquals(expResult, result);
    }

    /**
     * Test of doShow method, of class SignupRequestUI.
     */
    @Test
    public void testDoShow() {
        System.out.println("doShow");
        SignupRequestUI instance = new SignupRequestUI();
        boolean expResult = false;
        boolean result = instance.doShow();
        assertEquals(expResult, result);
    }

    /**
     * Test of headline method, of class SignupRequestUI.
     */
    @Test
    public void testHeadline() {
        System.out.println("headline");
        String head = "Sign Up";
        SignupRequestUI instance = new SignupRequestUI();
        String expResult = head;
        String result = instance.headline();
        assertEquals(expResult, result);
    }
    
}
