/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.user.consoleapp.presentation;

import eapli.ecafeteria.application.meals.BookingMealController;
import eapli.ecafeteria.domain.meals.Booking;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;
import eapli.util.DateTime;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
//import java.util.Date;
import java.util.Iterator;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.Date;
import java.util.List;

/**
 *
 * @author Bruno Costa
 */
public class BookingMealUI extends AbstractUI {

    private final BookingMealController theController = new BookingMealController();

    protected Controller controller() {
        return this.theController;
    }
    
    @Override
    protected boolean doShow() {
        String date = Console.readLine("Insert the meal date! (YYYY-MM-DD)");
           
        Date mealDate = null;
        
        try {
            mealDate = Date.valueOf(date);
        } catch (Exception e) {
            System.out.println("Data nao reconecida...");
            doShow();
        }
        
        
        List<Meal> meals =  (List<Meal>) theController.getMealsByDate(mealDate);
        
        if(meals.isEmpty()){
            System.out.println("Não existem refeições para esse dia...");
            doShow();
        }

        int i = -1;
        for (Meal meal : meals) {
            System.out.println( i+1 + " - " + meal.date()+ " " + meal.dish().name() + " " + meal.mealType().description() + " " + meal.toString());            
            i++;
        }
                     
        //for each
        int meal = Console.readInteger("Choose meal!(Number)");
        Booking booking = null;
        try {
            booking = theController.bookingMeal(meals.get(meal));
        } catch (DataConcurrencyException ex) {
            System.out.println("Occorreu um erro ao gravar na base de dados...");
            doShow();
        } catch (DataIntegrityViolationException ex) {
            System.out.println("Occorreu um erro ao gravar na base de dados...");
            doShow();
        }
        
        System.out.println("booking: " + booking.getMeal().dish().name() + " for " + booking.getMeal().date() + " was booked successfuly !");
        
        int response = Console.readInteger("Press 1 to book more meals, 0 to exit");
        
        while(response != 0 && response != 1){
             response = Console.readInteger("Press 1 to book more meals, 0 to exit");
        }
        
        if(response == 1)
            doShow();
        
        return false;
    }

    @Override
    public String headline() {
        return "Meal Booking";
    }
    
}
