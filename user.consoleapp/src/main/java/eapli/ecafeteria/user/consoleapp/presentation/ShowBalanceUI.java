/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.user.consoleapp.presentation;

import eapli.ecafeteria.application.CafeteriaUserBaseController;

/**
 *
 * @author jorge.esanto
 */
public class ShowBalanceUI extends CafeteriaUserBaseUI{

    @Override
    protected CafeteriaUserBaseController controller() {
        return new CafeteriaUserBaseController();
    }
    
    @Override
    protected boolean doShow() {
        System.out.println(super.showBalance());
        return true;
    }
    
}
