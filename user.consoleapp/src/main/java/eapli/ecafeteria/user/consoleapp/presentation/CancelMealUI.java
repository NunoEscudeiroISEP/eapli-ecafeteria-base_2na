/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.user.consoleapp.presentation;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.application.meals.CancelMealController;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.presentation.console.AbstractUI;
import java.util.Scanner;

/**
 *
 * @author Cláudia
 */
public abstract class CancelMealUI extends AbstractUI {

    private final CafeteriaUserRepository customerRepository = PersistenceContext.repositories().cafeteriaUsers(true);
    private final CancelMealController cancelController = new CancelMealController();
    SystemUser systemUser = Application.session().session().authenticatedUser();
    CafeteriaUser user = customerRepository.findByUsername(systemUser.username());
    
    
    protected String cancellingMeal() {
        System.out.println("The booking of the cliente are:" + cancelController.getAllBookings(user));

        Scanner scanner = new Scanner(System.in);
        System.out.println("Choosing a booking(Name)");
        String choosed=scanner.nextLine();
       
        
        return null;
    }

    @Override
    public String headline() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
